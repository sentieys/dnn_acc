#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef SIZE
#define SIZE 2048
#endif
#ifndef BLOCK1
#define BLOCK1 32
#endif

void checkMatrix(double *result, int size);
void initMatrix(double* A1, double* B1, double *C1, int size);

int main(int argc, char **argv){
	int size = SIZE;
	printf("size is %dx%d\n", size, size);

	double *A1 = malloc(size*size*sizeof(double));
	double *B1 = malloc(size*size*sizeof(double));
	double *result = malloc(size*size*sizeof(double));
	
	initMatrix(A1, B1,result, size);

	clock_t start, end;
    double cpu_time_used;
    start = clock();
	for (int i=0; i<size; i+=BLOCK1){
		for (int j=0; j<size; j+=BLOCK1){
			for (int k=0; k<size; k+=BLOCK1){
				for (int ii=0; ii<BLOCK1; ii+=1){
					for (int jj=0; jj<BLOCK1; jj+=1){
						for (int kk=0; kk<BLOCK1; kk+=1){
							result[(i+ii)*size+j+jj] += A1[(i+ii)*size+k+kk] * B1[(k+kk)*size+jj+j];
						}
					}
				}
			}
		}
	}
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("time: %f seconds\n", cpu_time_used);
	checkMatrix(result, size);
	return 0;
}


void initMatrix(double* A1, double* B1, double *C1, int size){

	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			A1[i*size+j] = 1;
			B1[i*size+j] = 1;
			C1[i*size+j] = 0;
		}
	}
}


void checkMatrix(double *result, int size){

	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			if (result[i*size+j] != size){
				printf("Error : value of result[%d][%d] is %f instead of %d !\n", i, j , result[i*size+j], size);
				return; 
			}
		}
	}

}
