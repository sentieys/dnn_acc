#include <stdio.h>
#include <string.h>
#include <string.h>
#define N 4
#define M 4
#define K 4


int main(void)
{

  int A[M][K] = {{7, 85, 14, 3}, 
                 {0, 1, 2, 20}, 
                 {7, 4, 3, 18}, 
                 {26, 75, 94, 47}};

  int B[K][N] = {{1, 2, 3, 4}, 
               {5, 6, 7, 8}, 
               {9, 10, 11, 12}, 
               {13, 14, 15, 16}};

  int C[M][N];
  //597, 706, 815, 924, 
  //283, 306, 329, 352, 
  //288, 320, 352, 384, 
  //1858, 2100, 2342, 2584

printf("BASELINE VERSION\n");   

  int i,j,k;
  int sum;

      for (i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            sum = 0;
            for (int k = 0; k < K; k++) {
                sum += A[i][k] * B[k][j];
            }
            C[i][j] = sum;
        }
    }

for (i = 0; i < M; ++i) {
  for (j = 0; j < N; ++j) {
    printf("%d, ", C[i][j]);
  }   
  printf("\n");   
}

printf("TILED VERSION\n");   
// TILED
for (i = 0; i < M; i++) 
  for (j = 0; j < N; j++) 
    C[i][j] = 0;

int it,jt,kt;
#define T 2

for (i=0; i<M/T; i+=1) // Tile row index
  for (j=0; j<N/T; j+=1) // Tile column index
    for (k=0; k<K/T; k+=1)
      for (it=0; it<T; it++)
        for (jt=0; jt<T; jt++)
          for (kt=0; kt<T; kt++) {
            //printf("i=%d,j=%d,k=%d,it=%d,jt=%d,kt=%d\n",i,j,k,it,jt,kt);
            C[i*T+it][j*T+jt] += A[i*T+it][k*T+kt] * B[k*T+kt][j*T+jt];

/*for (int ii = 0; ii < M; ii++) {
  for (int jj = 0; jj < N; jj++) {
    printf("%d, ", C[ii][jj]);
  }   
  printf("\n");   
}*/
          }


/*for (int i=0; i<size; i+=BLOCK1){
		for (int j=0; j<size; j+=BLOCK1){
			for (int k=0; k<size; k+=BLOCK1){
				for (int ii=0; ii<BLOCK1; ii+=1){
					for (int jj=0; jj<BLOCK1; jj+=1){
						for (int kk=0; kk<BLOCK1; kk+=1){
							result[(i+ii)*size+j+jj] += A1[(i+ii)*size+k+kk] * B1[(k+kk)*size+jj+j];
						}
					}
				}
			}
		}
	}*/




/*for (int m = 0; m < M/T; m += T) {
        for (int n = 0; n < N/T; n += T) {
            for (int k = 0; k < K/T; k += T) {
                for (int mt = m; mt < m + T && mt < M; mt++) {
                    for (int nt = n; nt < n + T && nt < N; nt++) {
                        for (int kt = k; kt < k + T && kt < K; kt++) {
                            C[mt * M + nt] += A[mt * M + kt] * B[kt * K + nt];
                        }
                    }
                }
            }
        }
    }*/


for (i = 0; i < M; ++i) {
  for (j = 0; j < N; ++j) {
    printf("%d, ", C[i][j]);
  }   
  printf("\n");   
}

  return 0;
}
