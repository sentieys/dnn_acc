library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity control_unit is
  port (clk: in std_logic;
        rstb: in std_logic;
        instruction: in instructions;
        output_ready: out std_logic;
        ctrl_signals: out std_logic_vector(0 to 11));
end control_unit;


architecture RTL of control_unit is

  signal CurrentState, NextState: states := wait_for_instruction;
  signal counter: integer range 0 to MAX_COUNT-1 := 0;
  signal enable_counter: std_logic := '0';
  signal clear_counter: std_logic := '0';

  alias loadA_SA: std_logic is ctrl_signals(0);
  alias loadB_SA: std_logic is ctrl_signals(1);
  alias loadPP_SA: std_logic is ctrl_signals(2);
  alias clearPP_SA: std_logic is ctrl_signals(3);
  alias push_IB: std_logic is ctrl_signals(4);
  alias pull_IB: std_logic is ctrl_signals(5);
  alias shift_SDS_IB: std_logic is ctrl_signals(6);
  alias push_OB: std_logic is ctrl_signals(7);
  alias pull_OB: std_logic is ctrl_signals(8);
  alias shift_SDS_OB: std_logic is ctrl_signals(9);
  alias push_WB: std_logic is ctrl_signals(10);
  alias pull_WB: std_logic is ctrl_signals(11);

begin
        -- counter CPE cycles
        CPT_CPE: process (rstb,clk)
        begin
                if clk'event AND clk='1' then
                        if clear_counter = '1' then
                                counter <= 0;
                        elsif enable_counter = '1' then
                                if counter = MAX_COUNT-1 then
                                        counter <= 0;
                                else
                                        counter <= counter + 1;
                                end if;
                        end if;
                end if;
        end process;

 	synchronous: process(clk, rstb)
        begin
                if rstb = '0' then
                        CurrentState <= wait_for_instruction;
                elsif clk'event AND clk = '1' then
                        CurrentState <= NextState;
                end if;
        end process;

        combinational: process(CurrentState, instruction, counter)
        begin
                -- instructions: Idle, Load_Weights_from_Host, Load_Input_Buffer_from_Host, Write_Weights_to_SA, 
                --               Matrix_Multiply, Activate, Write_Output_Buffer_to_Host
                case CurrentState is
                        when wait_for_instruction =>
                                ...
                        ...
                when others => -- this should never happen but is required to avoid latches
                                NextState <= wait_for_instruction;
                end case;
        end process;

        clear_counter <= '1' when CurrentState = ...
        ...

end RTL;  

