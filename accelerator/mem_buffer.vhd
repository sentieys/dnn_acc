library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

-- Memory Buffer (FIFO style)
entity mem_buffer is
    generic (Nword_size: integer := 32; 
             FIFO_DEPTH: integer := 16;
             NFIFO: integer := 8);
	port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          data_in: in Std_Logic_Vector(Nword_size-1 downto 0) := (others => '0'); 
          data_out: out vec_data_in(0 to NFIFO-1) := (others => (others=> '0')) );
end mem_buffer;

architecture RTL of mem_buffer is

    type mem_FIFO is array (0 to FIFO_DEPTH-1, 0 to NFIFO-1) of Std_Logic_Vector(Nbit-1 downto 0);

    signal FIFO: mem_FIFO := (others => (others => (others=> '0')));
    signal WR_INDEX: integer range 0 to FIFO_DEPTH-1 := 0;
    signal RD_INDEX: integer range 0 to FIFO_DEPTH-1 := 0;
    signal FIFO_COUNT: integer range 0 to FIFO_DEPTH := 0; -- # Words in FIFO
 
    signal w_FULL: std_logic;
    signal w_EMPTY: std_logic;

    signal low_high: std_logic; -- '0' write low part, '1' write high part

begin

  p_FIFO : process(clk) is
    begin
      if rising_edge(clk) then
        if rst_sync = '1' then
          ...
        else
   
          -- Keeps track of the total number of words in the FIFO
          if (push = '1' and pull = '0'and w_FULL = '0' and low_high = '1') then
            FIFO_COUNT <= FIFO_COUNT + 1;
          elsif (push = '0' and pull = '1'and w_EMPTY = '0') then
            FIFO_COUNT <= FIFO_COUNT - 1;
          end if;
   
          -- Keeps track of the write (push) index (and controls roll-over)
          if (push = '1' and w_FULL = '0' and low_high = '1') then
            ...
          end if;
   
          -- Keeps track of the read (pull) index (and controls roll-over)        
          if (pull = '1' and w_EMPTY = '0') then
            ...
          end if;
   
          -- Registers the input data when there is a write/push
          if push = '1' then
            ...
          end if;
           
        end if;                           -- sync reset
      end if;                             -- rising_edge(i_clk)
    end process p_FIFO;
    
    gen_data_out: for i in 0 to NFIFO-1 generate
        data_out(i) <= FIFO(RD_INDEX,i);        
    end generate ;     
 
    w_FULL  <= '1' when FIFO_COUNT = FIFO_DEPTH else '0';
    w_EMPTY <= '1' when FIFO_COUNT = 0       else '0';
   
    full  <= w_FULL;
    empty <= w_EMPTY;

    end RTL;
