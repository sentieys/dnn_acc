library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;
    
-- C = B * A, with B = Weights
entity SA is
    port (clk, rstb: in Std_Logic;
            load_A, load_B, load_PP, clear_PP: in Std_Logic;
            A_in: in vec_inputs := (others => (others=> '0'));    -- Inputs
            B_in: in vec_weights := (others => (others=> '0'));   -- Weights
            C_out: out vec_outputs := (others => (others=> '0'))  -- Outputs
        );	
end SA;

architecture RTL of SA is

type A_mat is array (0 to RPE-1, 0 to CPE) of Std_Logic_Vector(Nbit-1 downto 0);
type B_mat is array (0 to RPE, 0 to CPE-1) of Std_Logic_Vector(Nbit-1 downto 0);
type PP_mat is array (0 to RPE, 0 to CPE-1) of Std_Logic_Vector(Nbitacc-1 downto 0);
signal A: A_mat;
signal B: B_mat;
signal PP: PP_mat;

component PE is
    port ( A_in: in Std_Logic_Vector(Nbit-1 downto 0); -- Inputs
        B_in: in Std_Logic_Vector(Nbit-1 downto 0); -- Weights
        PP_in: in Std_Logic_Vector(Nbitacc-1 downto 0); -- Partial Product inputs
        A_out: out Std_Logic_Vector(Nbit-1 downto 0); -- Inputs forwarding
        B_out: out Std_Logic_Vector(Nbit-1 downto 0); -- Weightrs forwarding
        PP_out: out Std_Logic_Vector(Nbitacc-1 downto 0); -- Partial Product outputs
        loadA: in Std_Logic;
        loadB: in Std_Logic;
        loadPP: in Std_Logic;
        clearPP: in Std_Logic;
        clk: in Std_Logic );
end component PE;
	
begin
 
  ...
	
end RTL;
