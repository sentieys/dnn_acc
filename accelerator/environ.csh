setenv LM_LICENSE_FILE 5280@mentor.cnfm.fr

echo Configuring ModelSimSE SE-64 10.6c
setenv MODEL_TECH /usr/local/Modelsim/modeltech/bin
set path=(/usr/local/Modelsim/modeltech/bin $path)

echo Configuring Catapult2023.1
setenv MGC_HOME /usr/local/Siemens_EDA/Catapult_Synthesis_2023.1-1033555/Mgc_home
set path=(/usr/local/Siemens_EDA/Catapult_Synthesis_2023.1-1033555/Mgc_home/bin $path)

#echo Configuring Design Compiler 2010.03
#setenv LM_LICENSE_FILE $LM_LICENSE_FILE:27000@synopsys.cnfm.fr
#setenv SYNOPSYS /usr/local/DesignVision-2010.03-SP4
#set path=($SYNOPSYS/amd64/syn/bin $path)
#set path=($SYNOPSYS $path)

setenv LIBRARY_PATH /usr/lib/x86_64-linux-gnu
setenv LD_LIBRARY_PATH /usr/lib/x86_64-linux-gnu