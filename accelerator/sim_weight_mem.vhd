library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_WM is
end test_WM; 

architecture test of test_WM is

    signal W_host: mem_weights := (others => (others=> '0'));

    signal clk: std_logic := '0';
    signal rst_sync: std_logic := '1';
    signal rstb: std_logic := '0';
    signal push: std_logic := '0';
    signal pull: std_logic := '0';
    signal full: std_logic := '0';
    signal empty: std_logic := '0';
    signal w_in: Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); 
    signal w_out: vec_weights := (others => (others=> '0'));

	component weight_mem is
	port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          w_in: in Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); -- 32-bit (Nword) input
          w_out: out vec_weights := (others => (others=> '0')) );	-- vector of 8 (CPE) 8-bit (Nbit) weights
    end component weight_mem;

    for instance_WM:weight_mem use entity work.weight_mem(RTL);

begin

  instance_WM: weight_mem 
    PORT MAP (clk, rst_sync, push, pull, full, empty, 
              w_in, w_out);	

  clock: process
  begin
    clk <= '1';
    wait for clock_cycle/2;
    clk <= '0';
    wait for clock_cycle/2;
  end process;
  
  reset: process
  begin
    rstb <= '0';
	wait for clock_cycle;
    rstb <= '1';
    wait;
  end process;
  rst_sync <= not rstb;

  init_W_host: process
    variable j: integer;
  begin
    j:=0;
    for i in 0 to RPE-1 loop
        W_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(W(i,0), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,1), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,2), Nbit)) & 
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,3), Nbit));
        j := j+1;
        W_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(W(i,4), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,5), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,6), Nbit)) & 
                     STD_LOGIC_VECTOR(TO_SIGNED(W(i,7), Nbit));
        j := j+1;
    end loop;
    wait;
  end process;

  simulation: process 
  begin
    w_in <= (others=> '0');
    push <= '0';
	  pull <= '0';
    wait for clock_cycle*2;
    -- push (write) the W (weight) matrix from host
    for i in W_host'range loop
        push <= '1';
        w_in <= W_host(i);
		    wait for clock_cycle;
        report "w_in " & integer'image(i) & " : " & to_string(w_in);
    end loop;
    push <= '0';
	wait for clock_cycle;

    -- pull (write) the W (weight) FIFO to SA
    for i in 0 to RPE-1 loop
        report "i " & integer'image(i);
        pull <= '1';
        for j in 0 to CPE-1 loop
            report "w_out " & integer'image(j) & " : " & to_string(w_out(j));
        end loop;
        wait for clock_cycle;
    end loop;
    pull <= '0';
    wait for clock_cycle;
    
    wait;
  end process;

end test;



