library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.systolic_array_pkg.all;

entity dffs is
    port (clk, rst_sync, enable: in std_logic;
          d_in: Std_Logic_Vector(Nbit-1 downto 0);
          q_out: out Std_Logic_Vector(Nbit-1 downto 0) );
end dffs;
architecture RTL of dffs is
    begin
        process(clk) is 
        begin
            if rising_edge(clk) then
                if rst_sync = '1' then
                    q_out <= (others => '0');
                elsif enable = '1' then
                    q_out <= d_in;
                end if;
            end if;
        end process;
end RTL;

library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity systolic_data_setup is
	port (clk, rst_sync: in Std_Logic;
          shift: in Std_Logic;
          a_in: in vec_data_in(0 to RPE-1) := (others => (others=> '0')); -- vector of 8 (RPE) 8-bit (Nbit) data
          a_out: out vec_data_in(0 to RPE-1) := (others => (others=> '0')) ); -- vector of 8 (RPE) 8-bit (Nbit) data
end systolic_data_setup;

architecture RTL of systolic_data_setup is

    -- a_in(0) -|-> a_out(0)            with -|- a DFF
    -- a_in(1) -|-|-> a_out(1)
    -- a_in(2) -|-|-|-> a_out(2)
    -- a_in(3) -|-|-|-|-> a_out(3)
    -- ...
    -- a_in(7) -|-|-| ... |-|-> a_out(7)
    type dff_data_setup is array(0 to RPE-1, 0 to CPE) of Std_Logic_Vector(Nbit-1 downto 0);
    signal w_dff: dff_data_setup := (others => (others => (others=> '0'))); 

    component dffs is
        port (clk, rst_sync, enable: in std_logic;
          d_in: Std_Logic_Vector(Nbit-1 downto 0);
          q_out: out Std_Logic_Vector(Nbit-1 downto 0) );	
    end component dffs;

begin

    ...

end RTL;