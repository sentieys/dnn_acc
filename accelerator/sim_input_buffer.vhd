library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_IB is
end test_IB; 

architecture test of test_IB is

    signal A_host: mem_inputs := (others => (others=> '0'));

    signal clk: std_logic := '0';
    signal rst_sync: std_logic := '1';
    signal rstb: std_logic := '0';
    signal push: std_logic := '0';
    signal pull: std_logic := '0';
    signal full: std_logic := '0';
    signal empty: std_logic := '0';
    signal a_in: Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); 
    signal a_out: vec_data_in(0 to RPE-1) := (others => (others=> '0'));

	component input_buffer is
        port (clk, rst_sync: in Std_Logic;
              push, pull: in Std_Logic;
              full, empty: out Std_Logic;
              a_in: in Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); -- 32-bit (Nword) input
              a_out: out vec_data_in(0 to RPE-1) := (others => (others=> '0')) );	-- vector of 8 (RPE) 8-bit (Nbit) inputs
    end component input_buffer;

    for instance_IB:input_buffer use entity work.input_buffer(RTL);

begin

  instance_IB: input_buffer 
    port map (clk, rst_sync, push, pull, full, empty, 
              a_in, a_out);	

  clock: process
  begin
    clk <= '1';
    wait for clock_cycle/2;
    clk <= '0';
    wait for clock_cycle/2;
  end process;
  
  reset: process
  begin
    rstb <= '0';
	wait for clock_cycle;
    rstb <= '1';
    wait;
  end process;
  rst_sync <= not rstb;

  init_A_host: process
    variable j: integer;
  begin
    j:=0;
    for i in 0 to RPE-1 loop
        A_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(A(i,0), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,1), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,2), Nbit)) & 
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,3), Nbit));
        j := j+1;
        A_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(A(i,4), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,5), Nbit)) &
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,6), Nbit)) & 
                     STD_LOGIC_VECTOR(TO_SIGNED(A(i,7), Nbit));
        j := j+1;
    end loop;
    wait;
  end process;

  simulation: process 
  begin
    a_in <= (others=> '0');
    push <= '0';
	pull <= '0';
    wait for clock_cycle*2;
    -- push (write) the W (weight) matrix from host
    for i in 0 to 15 loop
        push <= '1';
        a_in <= A_host(i);
		    wait for clock_cycle;
        report "a_in " & integer'image(i) & " : " & to_string(a_in);
    end loop;
    push <= '0';
	wait for clock_cycle;

    -- pull (write) the W (weight) FIFO to SA
    for i in 0 to RPE-1 loop
        report "i " & integer'image(i);
        pull <= '1';
        for j in 0 to CPE-1 loop
            report "a_out " & integer'image(j) & " : " & to_string(a_out(j));
        end loop;
        wait for clock_cycle;
    end loop;
    pull <= '0';
    wait for clock_cycle;
    
    wait;
  end process;

end test;



