library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_SA is
end test_SA; 

architecture test of test_SA is

    signal simok: bit := '1';
    signal start,ready: bit := '0';

    signal clk: std_logic := '0';
    signal rstb: std_logic := '0';
    signal load_A: std_logic := '0';
    signal load_B: std_logic := '0';
    signal load_PP: std_logic := '0';
    signal clear_PP: std_logic := '0';
    signal A_in: vec_inputs := (others => (others=> '0')); 
    signal B_in: vec_weights := (others => (others=> '0')); 
    signal C_out: vec_outputs := (others => (others=> '0'));

	component SA is
        port (clk, rstb: in Std_Logic;
            load_A, load_B, load_PP, clear_PP: in Std_Logic;
            A_in: in vec_inputs := (others => (others=> '0'));    -- Inputs
            B_in: in vec_weights := (others => (others=> '0'));   -- Weights
            C_out: out vec_outputs := (others => (others=> '0'))  -- Outputs
        );		
    end component SA;

    for instance_SA:SA use entity work.SA(RTL);

begin

  instance_SA: SA 
    PORT MAP (clk, rstb, load_A, load_B, load_PP, clear_PP, 
              A_in, B_in, C_out);	

  clock: process
  begin
    clk <= '1';
    wait for clock_cycle/2;
    clk <= '0';
    wait for clock_cycle/2;
  end process;
  
  reset: process
  begin
    rstb <= '0';
	  wait for clock_cycle;
    rstb <= '1';
    wait;
  end process;

  simulation: process 
    variable tmp: std_logic_vector(Nbit-1 downto 0) := (others => '0');
    variable row,col: integer := 0;
  begin
    A_in <= (others => (others=> '0'));
    B_in <= (others => (others=> '0'));
    load_A <= '0';
    load_B <= '0';
    load_PP <= '0';
    clear_PP <= '1';
    start <= '0';
    wait for clock_cycle*2;
    -- send the W (weight) matrix
    for i in RPE-1 downto 0 loop
        for j in 0 to CPE-1 loop
            tmp := STD_LOGIC_VECTOR(TO_SIGNED(W(i,j), Nbit));
            B_in(j) <= tmp;
            --report "W(" & integer'image(i) & "," & integer'image(j) & ") = " & to_string(tmp);
            --report "W(" & integer'image(i) & "," & integer'image(j) & ") = " & integer'image(W(i,j));
        end loop;
        load_B <= '1'; -- load W(i)
		wait for clock_cycle;
    end loop;
    B_in <= (others => (others=> '0'));
	  wait for clock_cycle;
    load_B <= '0';
	  wait for clock_cycle;

    -- send the A (inputs) matrix
    start <= '1';
    load_A <= '1';
    load_PP <= '1';
    clear_PP <= '0';
    for i in 0 to CPE+RPE-1 loop
        for j in 0 to CPE-1 loop
            tmp := STD_LOGIC_VECTOR(TO_SIGNED(A_FIFO(i,j), Nbit));
            A_in(j) <= tmp;
            --report "A_in(" & integer'image(j) & ") = " & integer'image(TO_INTEGER(SIGNED(A_in(j))));
        end loop;
		    wait for clock_cycle;
        if i=RPE then
            ready <= '1';
        end if;
    end loop;

    wait for CPE*clock_cycle;

    report "end of simulation";
    -- if simok = '1' then
    --   report "ALL TESTS PASSED";
    -- else
    --   report "PROBLEM DURING SIMULATION";
    -- end if;
   	wait;
  end process;

  printC: Process (clk,ready)
  begin
    if clk'event and clk='1' and ready='1' then
        for i in C_out'range loop
            report "C_out(" & integer'image(i) & ") = " & integer'image(TO_INTEGER(SIGNED(C_out(i))));
        end loop;
    end if;
  end process;

--   spy: Process (clk)
--   begin
--     if clk'event and clk='1' then
--       if start = '1' then
--         assert (y_int = test_y) 
--  	      report  "Sample " & integer'image(sample) & ": Problem on output:" & " yn = " & integer'image(y_int) & "; expected yn = " & integer'image(test_y)
--           severity warning;
--         if y_int = test_y then
--          report "Sample " & integer'image(sample) & ": Output ok:" & " yn = " & integer'image(y_int);
--         else
--          simok <= '0';
--         end if; 
--       end if; 
--     end if;
--   end process;
end test;



