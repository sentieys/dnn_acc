library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

-- Weight Stationary Processing Element (PE) 
entity PE is
  port ( A_in: in Std_Logic_Vector(Nbit-1 downto 0); -- Inputs
         B_in: in Std_Logic_Vector(Nbit-1 downto 0); -- Weights
         PP_in: in Std_Logic_Vector(Nbitacc-1 downto 0); -- Partial Product inputs
         A_out: out Std_Logic_Vector(Nbit-1 downto 0); -- Inputs forwarding
         B_out: out Std_Logic_Vector(Nbit-1 downto 0); -- Weights forwarding
         PP_out: out Std_Logic_Vector(Nbitacc-1 downto 0); -- Partial Product outputs
         loadA: in Std_Logic;
         loadB: in Std_Logic;
         loadPP: in Std_Logic;
         clearPP: in Std_Logic;
         clk: in Std_Logic );
end PE;

----------------------------------------------------------------------

architecture RTL OF PE is

 signal A : SIGNED(Nbit-1 downto 0) :=  (others => '0'); -- Register A 
 signal B : SIGNED(Nbit-1 downto 0) :=  (others => '0'); -- Register B
 signal PP : SIGNED(Nbitacc-1 downto 0) :=  (others => '0'); -- Register PP 


begin

  ...

end RTL;

