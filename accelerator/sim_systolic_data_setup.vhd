library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_DS is
end test_DS; 

architecture test of test_DS is

    signal clk: std_logic := '0';
    signal rst_sync: std_logic := '1';
    signal rstb: std_logic := '0';
    signal shift: std_logic := '0';
    signal a_in: vec_data_in(0 to RPE-1) := (others => (others=> '0')); 
    signal a_out: vec_data_in(0 to RPE-1) := (others => (others=> '0'));

	component systolic_data_setup is
        port (clk, rst_sync: in Std_Logic;
                shift: in Std_Logic;
                a_in: in vec_data_in(0 to RPE-1) := (others => (others=> '0')); 
                a_out: out vec_data_in(0 to RPE-1) := (others => (others=> '0')) );	
    end component systolic_data_setup;
    
    for instance_DS:systolic_data_setup use entity work.systolic_data_setup(RTL);

begin

    instance_DS: systolic_data_setup
    port map (clk, rst_sync, shift, a_in, a_out);


    clock: process
    begin
        clk <= '1';
        wait for clock_cycle/2;
        clk <= '0';
        wait for clock_cycle/2;
    end process;
    
    reset: process
    begin
        rstb <= '0';
        wait for clock_cycle;
        rstb <= '1';
        wait;
    end process;
    rst_sync <= not rstb;


    simulation: process 
    begin
        a_in <= (others => (others=> '0'));
        shift <= '0';
        wait for clock_cycle*2;
        wait for clock_cycle/2;
        for i in 0 to 15 loop
            shift <= '1';
            for j in 0 to 7 loop
                a_in(j) <= STD_LOGIC_VECTOR(TO_SIGNED(A(i,j), Nbit));
            end loop;
            wait for clock_cycle;
            --report "a_in " & integer'image(i) & " : " & to_string(a_in);
        end loop;
        shift <= '0';
        wait for clock_cycle;
        
        wait;
    end process;


    print_output: process (clk)
        variable cycle: integer:= 0;
    begin
        if clk'event and clk='1' then
            report "cycle " & integer'image(cycle);
            for i in a_out'range loop
                report "a_out(" & integer'image(i) & ") = " & integer'image(TO_INTEGER(SIGNED(a_out(i))));
            end loop;
            cycle := cycle+1;
        end if;
    end process;
    
end test;



