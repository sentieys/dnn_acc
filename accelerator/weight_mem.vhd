library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

-- Weights Memory
entity weight_mem is
	port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          w_in: in Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); -- 32-bit (Nword) input
          w_out: out vec_data_in(0 to CPE-1) := (others => (others=> '0')) );	-- vector of 8 (CPE) 8-bit (Nbit) weights
end weight_mem;

architecture RTL of weight_mem is

  component mem_buffer is
    generic (Nword_size: integer := 32; 
             FIFO_DEPTH: integer := 16;
             NFIFO: integer := 8);
	  port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          data_in: in Std_Logic_Vector(Nword_size-1 downto 0) := (others => '0'); 
          data_out: out vec_data_in(0 to NFIFO-1) := (others => (others=> '0')) );
  end component mem_buffer;

  for instance_MB:mem_buffer use entity work.mem_buffer(RTL);

begin

  instance_MB: mem_buffer
    generic map (Nword, weight_mem_size, CPE)
    PORT MAP (clk, rst_sync, push, pull, full, empty, 
              w_in, w_out);	

end RTL;
