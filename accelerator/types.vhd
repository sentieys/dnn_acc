library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

package systolic_array_pkg is

   constant CPE: integer := 8; -- number of PEs per column
   constant RPE: integer := 8; -- number of PEs per row
   constant PE: integer := CPE*RPE; -- number of PEs (64)
   constant M: integer := 8;
   constant N: integer := 8;
   -- weight and input matrix size is MxN

   constant Nword: integer := 32; -- size (in bits) of a memory line
   constant Nbit: integer := 8; -- size (in bits) of weights and activations
   constant Nbitacc: integer := 16; -- size (in bits) of partial products
   constant weight_mem_size: integer := M*N*Nbit/Nword; -- size of weight memory is weight_mem_size*Nword (16x32)
   constant Nweights: integer := M*N; -- number of weights in memory (64)
   constant input_buffer_size: integer := 4*N; -- size of input buffer to store 4 input matrices

   type mem_weights is array (0 to weight_mem_size-1) of Std_Logic_Vector(Nword-1 downto 0);
   type mem_inputs is array (0 to input_buffer_size-1) of Std_Logic_Vector(Nword-1 downto 0);

   type matrix is array (0 to 2*M-1, 0 to N-1) of integer range -2**(Nbit-1) to +2**(Nbit-1)-1;
   type matrixC is array (0 to 4*M-1, 0 to N-1) of integer range -2**(Nbitacc-1) to +2**(Nbitacc-1)-1;
   type vector is array (0 to N-1) of integer range -2**(Nbit-1) to +2**(Nbit-1)-1;

   type vec_data_in is array (integer range <>) of Std_Logic_Vector(Nbit-1 downto 0);
   subtype vec_inputs is vec_data_in(0 to RPE-1);
   subtype vec_weights is vec_data_in(0 to CPE-1);
   type vec_outputs is array (0 to CPE-1) of Std_Logic_Vector(Nbitacc-1 downto 0);

   type instructions is  (Idle, Load_Weights_from_Host, Load_Input_Buffer_from_Host, Write_Weights_to_SA, 
                          Matrix_Multiply, Activate, Write_Output_Buffer_to_Host);
   type states is (wait_for_instruction, Load_WBfH, Load_IBfH, Write_WBtSA, MM0, MM1, MM2, Write_OBtH);
   constant MAX_COUNT: integer := 32;

   -- for simulation
	constant clock_cycle: time := 5 ns;


   constant W: matrix := ( 
      (1,2,3,4,5,6,7,8),
      (9,10,11,12,13,14,15,16),
      (17,18,19,20,21,22,23,24),
      (25,26,27,28,29,30,31,32),
      (-25,-26,-27,-28,-29,-30,-31,-32),
      (17,18,19,20,21,22,23,24),
      (-9,-10,-11,-12,-13,-14,-15,-16),
      (-1,-2,-3,-4,-5,-6,-7,-8), 
      others => (others => 0)
   );

   constant A: matrix := ( 
      -- (0,0,0,0,0,0,0,0),
      -- (0,0,0,0,0,0,0,0),
      -- (0,0,0,0,0,0,0,0),
      -- (0,0,0,0,0,0,0,0),
      -- (0,0,0,0,0,0,0,0),
      (1,9,17,25,33,41,49,57),
      (2,10,18,26,34,42,50,58),
      (3,11,19,27,35,43,51,59),
      (4,12,20,28,36,44,52,60),
      (5,13,21,29,37,45,53,61),
      (6,14,22,30,38,46,54,62),
      (7,15,23,31,39,47,55,63),
      (8,16,24,32,40,48,56,64),
       others => (others => 0)
   );
   
   constant A_FIFO: matrix := ( 
      (1,0,0,0,0,0,0,0), 
      (2,9,0,0,0,0,0,0),
      (3,10,17,0,0,0,0,0),
      (4,11,18,25,0,0,0,0),
      (5,12,19,26,33,0,0,0),
      (6,13,20,27,34,41,0,0),
      (7,14,21,28,35,42,49,0),
      (8,15,22,29,36,43,50,57),
      (0,16,23,30,37,44,51,58), 
      (0, 0,24,31,38,45,52,59), 
      (0, 0, 0,32,39,46,53,60), 
      (0, 0, 0, 0,40,47,54,61), 
      (0, 0, 0, 0, 0,48,55,62), 
      (0, 0, 0, 0, 0, 0,56,63), 
      (0, 0, 0, 0, 0, 0, 0,64), 
      (0, 0, 0, 0, 0, 0, 0, 0)
   );

   constant C: matrixC := (
      (370,324,278,232,186,140,94,48),
      (404,360,316,272,228,184,140,96),
      (438,396,354,312,270,228,186,144),
      (472,432,392,352,312,272,232,192),
      (506,468,430,392,354,316,278,240),
      (540,504,468,432,396,360,324,288),
      (574,540,506,472,438,404,370,336),
      (608,576,544,512,480,448,416,384),
      others => (others => 0)
   );

   constant C_FIFO: matrixC := (
      (370,0,0,0,0,0,0,0),
      (404,324,0,0,0,0,0,0),
      (438,360,278,0,0,0,0,0),
      (472,396,316,232,0,0,0,0),
      (506,432,354,272,186,0,0,0),
      (540,468,392,312,228,140,0,0),
      (574,504,430,352,270,184,94,0),
      (608,540,468,392,312,228,140,48),
      (0,576,506,432,354,272,186,96),
      (0,0,544,472,396,316,232,144),
      (0,0,0,512,438,360,278,192),
      (0,0,0,0,480,404,324,240),
      (0,0,0,0,0,448,370,288),
      (0,0,0,0,0,0,416,336),
      (0,0,0,0,0,0,0,384),
      (370,0,0,0,0,0,0,0),
      (404,324,0,0,0,0,0,0),
      (438,360,278,0,0,0,0,0),
      (472,396,316,232,0,0,0,0),
      (506,432,354,272,186,0,0,0),
      (540,468,392,312,228,140,0,0),
      (574,504,430,352,270,184,94,0),
      (608,540,468,392,312,228,140,48),
      (0,576,506,432,354,272,186,96),
      (0,0,544,472,396,316,232,144),
      (0,0,0,512,438,360,278,192),
      (0,0,0,0,480,404,324,240),
      (0,0,0,0,0,448,370,288),
      (0,0,0,0,0,0,416,336),
      (0,0,0,0,0,0,0,384),
      (0, 0, 0, 0, 0, 0, 0, 0),
      (0, 0, 0, 0, 0, 0, 0, 0)
   );

end systolic_array_pkg;

