library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_top is
end test_top; 

architecture test of test_top is

    signal clk: std_logic := '0';
    signal rstb: std_logic := '0';
    signal instruction: instructions;
    --Idle, Load_Weights_from_Host, Load_Input_Buffer_from_Host, Write_Weights_to_SA, 
    --Matrix_Multiply, Activate, Write_Output_Buffer_to_Host
    signal status: Std_Logic_Vector(6 downto 0) := (others => '0');
    alias IB_full: std_logic is status(0);
    alias IB_empty: std_logic is status(1);
    alias OB_full: std_logic is status(2);
    alias OB_empty: std_logic is status(3);
    alias WB_full: std_logic is status(4);
    alias WB_empty: std_logic is status(5);
    alias output_ready: std_logic is status(6);

    signal simok: bit := '1';

    signal from_Host: Std_Logic_Vector(Nword-1 downto 0) := (others => '0');
    signal to_Host: Std_Logic_Vector(Nword-1 downto 0) := (others => '0');
    signal C_out: vec_outputs := (others => (others=> '0')); -- during debug

    signal W_host: mem_weights := (others => (others=> '0'));
    signal A_host: mem_inputs := (others => (others=> '0'));

	component top_SA is
        port (clk, rstb: in Std_Logic;
              instruction: in instructions;
              status: out Std_Logic_Vector(6 downto 0);
              from_Host: in Std_Logic_Vector(Nword-1 downto 0); 
              C_out: out vec_outputs := (others => (others=> '0')); -- during debug
              to_Host: out Std_Logic_Vector(Nword-1 downto 0) );
    end component top_SA;

    for instance_top:top_SA use entity work.top_SA(RTL);

begin

  instance_top: top_SA 
    port map (clk, rstb, instruction, status, from_Host, C_out, to_Host);	--during debug

  clock: process
  begin
    clk <= '1';
    wait for clock_cycle/2;
    clk <= '0';
    wait for clock_cycle/2;
  end process;
  
  reset: process
  begin
    rstb <= '0';
	  wait for clock_cycle;
    rstb <= '1';
    wait;
  end process;

    init_W_host: process
        variable j: integer;
    begin
        j:=0;
        for i in 0 to RPE-1 loop
            W_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(W(i,4), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,5), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,6), Nbit)) & 
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,7), Nbit));
            j := j+1;
            W_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(W(i,0), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,1), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,2), Nbit)) & 
                        STD_LOGIC_VECTOR(TO_SIGNED(W(i,3), Nbit));
            j := j+1;
        end loop;
        wait;
    end process;

    init_A_host: process
        variable j: integer;
    begin
        j:=0;
        for i in 0 to CPE-1 loop
            A_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(A(i,0), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,1), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,2), Nbit)) & 
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,3), Nbit));
            j := j+1;
            A_host(j) <= STD_LOGIC_VECTOR(TO_SIGNED(A(i,4), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,5), Nbit)) &
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,6), Nbit)) & 
                        STD_LOGIC_VECTOR(TO_SIGNED(A(i,7), Nbit));
            j := j+1;
        end loop;
        wait;
    end process;

  simulation: process 
    variable tmp: std_logic_vector(Nbit-1 downto 0) := (others => '0');
    variable row,col: integer := 0;
  begin
    instruction <= Idle;
    from_Host <= (others => '0');
    wait for clock_cycle*2;
    
    -- progress simulation by half clock cycle to send commands on falling edge
    wait for clock_cycle/2; 
    
    -- Load_Weights_from_Host
    instruction <= Load_Weights_from_Host;
    wait for clock_cycle;
    for i in 15 downto 1 loop -- should be generic
        from_Host <= W_host(i);
		wait for clock_cycle;
        report "from_Host : a_in " & integer'image(i) & " : " & to_string(from_Host);
    end loop;
    from_Host <= W_host(0);
    instruction <= Idle;
    wait for clock_cycle;
    report "from_Host : w_in " & integer'image(0) & " : " & to_string(from_Host);
	
    wait for 3*clock_cycle;

    -- Load_Input_Buffer_from_Host
    instruction <= Load_Input_Buffer_from_Host;
    wait for clock_cycle;
    for i in 0 to 14 loop -- should be generic
        from_Host <= A_host(i);
        wait for clock_cycle;
        report "from_Host : a_in " & integer'image(i) & " : " & to_string(from_Host);
    end loop;
    instruction <= Idle;
    from_Host <= A_host(15);
    wait for clock_cycle;
    report "from_Host : a_in " & integer'image(15) & " : " & to_string(from_Host);

    wait for 3*clock_cycle;

    -- Write_Weights_to_SA
    instruction <= Write_Weights_to_SA;
    wait for clock_cycle;
    for i in 0 to 6 loop -- should be generic
        wait for clock_cycle;
        --report "from_Host : w_in " & integer'image(i) & " : " & to_string(<<SIGNAL .test_top.instance_top.B_in : natural>>);
    end loop;
    instruction <= Idle;
    wait for 3*clock_cycle;

    -- Matrix_Multiply
    instruction <= Matrix_Multiply;
    wait for clock_cycle;
    for i in 0 to 23 loop
        wait for clock_cycle;
    end loop;

    instruction <= Idle;
    wait for 3*clock_cycle;

    -- Load_Input_Buffer_from_Host
    instruction <= Load_Input_Buffer_from_Host;
    wait for clock_cycle;
    for i in 0 to 14 loop -- should be generic
        from_Host <= A_host(i);
        wait for clock_cycle;
        report "from_Host : a_in " & integer'image(i) & " : " & to_string(from_Host);
    end loop;
    instruction <= Idle;
    from_Host <= A_host(15);
    wait for clock_cycle;
    report "from_Host : a_in " & integer'image(15) & " : " & to_string(from_Host);

    wait for 3*clock_cycle;

    -- Matrix_Multiply
    instruction <= Matrix_Multiply;
    wait for clock_cycle;
    for i in 0 to 23 loop
        wait for clock_cycle;
    end loop;

    instruction <= Idle;
    wait for 3*clock_cycle;

    report "end of simulation";
    if simok = '1' then
      report "ALL TESTS PASSED";
    else
      report "PROBLEM DURING SIMULATION";
    end if;

    wait;
  end process;

    -- Debug - Not synthesized
    -- p_debug : process (clk) is
    -- begin
    --     if rising_edge(clk) then
    --     if instruction = Matrix_Multiply AND output_ready = '1' then
    --         for j in 0 to CPE-1 loop
    --             report "C_out " & integer'image(j) & " : " & integer'image(TO_INTEGER(SIGNED(C_out(j)))) & " : " & to_string(C_out(j));
    --         end loop;         
    --     end if;
    --     end if;
    -- end process p_debug;

  spy: Process (clk)
    variable i: integer := 0;
    variable C_tmp, test_C: integer;
  begin
    if clk'event and clk='1' then
        if output_ready = '1' then
            report  "#C_out:" & integer'image(i);
            for j in 0 to CPE-1 loop
                C_tmp := TO_INTEGER(SIGNED(C_out(j)));
                test_C := C_FIFO(i,j);
                assert (C_tmp = test_C) 
                    report  "C(" & integer'image(i) & "," & integer'image(j) & "): Problem on output:" & integer'image(C_tmp) & "; expected: " & integer'image(test_C)
                    severity warning;
                if C_tmp = test_C then
                    report "C(" & integer'image(i) & "," & integer'image(j) & "): Output ok:" & integer'image(C_tmp);
                else
                    simok <= '0';
                end if; 
            end loop;
            i := i+1;
        end if; 
    end if;
  end process;

end test;
