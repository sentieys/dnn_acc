library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity top_SA is
	port (clk, rstb: in Std_Logic;
          instruction: in instructions;
          status: out Std_Logic_Vector(6 downto 0) := (others => '0');
          from_Host: in Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); -- 32-bit (Nword) input
          C_out: out vec_outputs := (others => (others=> '0')); -- during debug
          to_Host: out Std_Logic_Vector(Nword-1 downto 0) := (others => '0') );	-- 32-bit (Nword) output
end top_SA;

architecture RTL of top_SA is

    signal data_int: vec_data_in(0 to RPE-1) := (others => (others=> '0'));

    signal ctrl_signals: std_logic_vector(0 to 11) := (others=> '0');
    alias loadA_SA: std_logic is ctrl_signals(0);
    alias loadB_SA: std_logic is ctrl_signals(1);
    alias loadPP_SA: std_logic is ctrl_signals(2);
    alias clearPP_SA: std_logic is ctrl_signals(3);
    alias push_IB: std_logic is ctrl_signals(4);
    alias pull_IB: std_logic is ctrl_signals(5);
    alias shift_SDS_IB: std_logic is ctrl_signals(6);
    alias push_OB: std_logic is ctrl_signals(7);
    alias pull_OB: std_logic is ctrl_signals(8);
    alias shift_SDS_OB: std_logic is ctrl_signals(9);
    alias push_WB: std_logic is ctrl_signals(10);
    alias pull_WB: std_logic is ctrl_signals(11);
   
    alias IB_full: std_logic is status(0);
    alias IB_empty: std_logic is status(1);
    alias OB_full: std_logic is status(2);
    alias OB_empty: std_logic is status(3);
    alias WB_full: std_logic is status(4);
    alias WB_empty: std_logic is status(5);
    alias output_ready: std_logic is status(6);

    signal rst_sync: std_logic := '1';

    signal A_in: vec_inputs := (others => (others=> '0')); 
    signal B_in: vec_weights := (others => (others=> '0')); 
    --signal C_out: vec_outputs := (others => (others=> '0')); -- during debug


	component control_unit is
        port (clk: in std_logic;
              rstb: in std_logic;
              instruction: in instructions;
              output_ready: out std_logic;
              ctrl_signals: out std_logic_vector(0 to 11));
    end component control_unit;

	component SA is
        port (clk, rstb: in Std_Logic;
            load_A, load_B, load_PP, clear_PP: in Std_Logic;
            A_in: in vec_inputs := (others => (others=> '0'));    
            B_in: in vec_weights := (others => (others=> '0'));  
            C_out: out vec_outputs := (others => (others=> '0'))  
        );		
    end component SA;

    component mem_buffer is
        generic (Nword_size: integer := 32; 
                 FIFO_DEPTH: integer := 16;
                 NFIFO: integer := 8);
          port (clk, rst_sync: in Std_Logic;
              push, pull: in Std_Logic;
              full, empty: out Std_Logic;
              data_in: in Std_Logic_Vector(Nword_size-1 downto 0); 
              data_out: out vec_data_in(0 to NFIFO-1) );
    end component mem_buffer;
    
    component systolic_data_setup is
        port (clk, rst_sync: in Std_Logic;
                shift: in Std_Logic;
                a_in: in vec_data_in(0 to RPE-1); 
                a_out: out vec_data_in(0 to RPE-1) );	
    end component systolic_data_setup;
    
    for input_buffer:mem_buffer use entity work.mem_buffer(RTL);
    for input_SDS:systolic_data_setup use entity work.systolic_data_setup(RTL);
    for weight_mem:mem_buffer use entity work.mem_buffer(RTL);
    for instance_SA:SA use entity work.SA(RTL);
    for instance_CU:control_unit use entity work.control_unit(RTL);

begin 

    rst_sync <= not rstb;

    input_buffer: mem_buffer
    generic map (Nword, input_buffer_size, RPE)
    port map (clk, rst_sync, push_IB, pull_IB, IB_full, IB_empty, from_Host, data_int);	

    input_SDS: systolic_data_setup
    port map (clk, rst_sync, shift_SDS_IB, data_int, A_in);

    weight_mem: mem_buffer
    generic map (Nword, weight_mem_size, CPE)
    PORT MAP (clk, rst_sync, push_WB, pull_WB, WB_full, WB_empty, from_Host, B_in);	

    instance_SA: SA 
    port map (clk, rstb, loadA_SA, loadB_SA, loadPP_SA, clearPP_SA, A_in, B_in, C_out);	

    instance_CU: control_unit 
    port map (clk, rstb, instruction, output_ready, ctrl_signals);	

    -- Debug - Not synthesized
    -- synthesis translate_off
    -- p_debug : process (clk) is
    -- begin
    --   if rising_edge(clk) then
    --     if instruction = Matrix_Multiply then
    --         for j in 0 to CPE-1 loop
    --             report "C_out " & integer'image(j) & " : " & integer'image(TO_INTEGER(SIGNED(C_out(j)))) & " : " & to_string(C_out(j));
    --         end loop;         
    --     end if;
    --   end if;
    -- end process p_debug;
    -- synthesis translate_on

end RTL;  
