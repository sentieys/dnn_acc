library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

-- Input FIFO Memory
entity input_buffer is
	port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          a_in: in Std_Logic_Vector(Nword-1 downto 0) := (others => '0'); -- 32-bit (Nword) input
          a_out: out vec_data_in(0 to RPE-1) := (others => (others=> '0')) );	-- vector of 8 (RPE) 8-bit (Nbit) inputs
end input_buffer;

architecture RTL of input_buffer is

  component mem_buffer is
    generic (Nword_size: integer := 32; 
             FIFO_DEPTH: integer := 16;
             NFIFO: integer := 8);
	  port (clk, rst_sync: in Std_Logic;
          push, pull: in Std_Logic;
          full, empty: out Std_Logic;
          data_in: in Std_Logic_Vector(Nword_size-1 downto 0) := (others => '0'); 
          data_out: out vec_data_in(0 to NFIFO-1) := (others => (others=> '0')) );
  end component mem_buffer;

  signal data_int: vec_data_in(0 to RPE-1) := (others => (others=> '0'));

  component systolic_data_setup is
    port (clk, rst_sync: in Std_Logic;
            pull: in Std_Logic;
            a_in: in vec_data_in(0 to RPE-1) := (others => (others=> '0')); 
            a_out: out vec_data_in(0 to RPE-1) := (others => (others=> '0')) );	
  end component systolic_data_setup;

  for instance_MB:mem_buffer use entity work.mem_buffer(RTL);
  for instance_DS:systolic_data_setup use entity work.systolic_data_setup(RTL);

begin

  instance_MB: mem_buffer
    generic map (Nword, input_buffer_size, RPE)
    port map (clk, rst_sync, push, pull, full, empty, 
              a_in, data_int);	

  instance_DS: systolic_data_setup
    port map (clk, rst_sync, pull, data_int, a_out);

    end RTL;
