library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.systolic_array_pkg.all;

entity test_CU is
end test_CU; 

architecture test of test_CU is

    signal clk: std_logic := '0';
    signal rstb: std_logic := '0';
    signal instruction: instructions;
    --Idle, Load_Weights_from_Host, Load_Input_Buffer_from_Host, Write_Weights_to_SA, 
    --Matrix_Multiply, Activate, Write_Output_Buffer_to_Host

    signal ctrl_signals: std_logic_vector(0 to 11);
    alias loadA_SA: std_logic is ctrl_signals(0);

    signal output_ready: std_logic := '0';

	component control_unit is
        port (clk: in std_logic;
              rstb: in std_logic;
              instruction: in instructions;
              output_ready: out std_logic;
              ctrl_signals: out std_logic_vector(0 to 11));
    end component control_unit;

    for instance_CU:control_unit use entity work.control_unit(RTL);

begin

  instance_CU: control_unit 
    port map (clk, rstb, instruction, output_ready, ctrl_signals);	

  clock: process
  begin
    clk <= '1';
    wait for clock_cycle/2;
    clk <= '0';
    wait for clock_cycle/2;
  end process;
  
  reset: process
  begin
    rstb <= '0';
	  wait for clock_cycle;
    rstb <= '1';
    wait;
  end process;

  simulation: process 
    variable tmp: std_logic_vector(Nbit-1 downto 0) := (others => '0');
    variable row,col: integer := 0;
  begin
    instruction <= Idle;
    wait for clock_cycle*2;

    -- Load_Weights_from_Host
    for i in 0 to 15 loop
        instruction <= Load_Weights_from_Host;
		wait for clock_cycle;
    end loop;
    instruction <= Idle;
	wait for 3*clock_cycle;

    -- Load_Input_Buffer_from_Host
    for i in 0 to 15 loop
        instruction <= Load_Input_Buffer_from_Host;
        wait for clock_cycle;
    end loop;
    instruction <= Idle;
    wait for 3*clock_cycle;

    -- Write_Weights_to_SA
    for i in 0 to 7 loop
        instruction <= Write_Weights_to_SA;
        wait for clock_cycle;
    end loop;
    instruction <= Idle;
    wait for 3*clock_cycle;

    -- Matrix_Multiply
    for i in 0 to 17 loop
        instruction <= Matrix_Multiply;
        wait for clock_cycle;
    end loop;
    instruction <= Idle;
    wait for 3*clock_cycle;

    wait;
  end process;
end test;



