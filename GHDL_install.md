
# GHDL

GHDL is an open-source simulator for the VHDL language. GHDL allows you to compile and execute your VHDL code directly in your PC on Windows, Linux or MacOS. Combined with a GUI-based wave viewer (GTKWave) and a good VHDL text editor (gedit, emacs, etc.), GHDL is a very powerful tool for writing, testing and simulating your VHDL code.

## Installation 
See <https://ghdl.github.io/ghdl/getting.html>. Package managers of many popular distributions provide pre-built packages of GHDL. This is the case for apt (Debian/Ubuntu), dnf (Fedora), pacman (Arch Linux, MSYS2) or brew (macOS). 

* Linux: `sudo apt-get install ghdl gtkwave`
* Mac OS X: `brew install --cask ghdl gtkwave` ou `sudo port install ghdl gtkwave`
* Windows: [installer](http://ghdl.free.fr/ghdl-installer-0.29.1.exe) (newer versions might exist)

# Simulating with GHDL

Compiling and analyzing vhdl files (design and testbench) with `ghdl -a <file.vhdl>`.
Elaborating the testbench with `ghdl -e <test_name>`.
Launching simulation with `ghdl -r <test_name> --stop-time=50ns --vcd=<vcd_file.vcd>`. Simulation results are dumped into a vcd file.

The full script to simulate the Half Adder is given below.

```
cd dnn_acc/VHDL
ghdl -a HalfAdder.vhdl 
ghdl -a sim_HalfAdder.vhdl
ghdl -e test_ha
ghdl -r test_ha --stop-time=50ns --vcd=sim_HalfAdder.vcd
gtkwave sim_HalfAdder.vcd 
```

The User Guide of GHDL is [here](https://ghdl.github.io/ghdl).  


# Displaying waveforms with [GTKWave](http://gtkwave.sourceforge.net/)

* Launch `gtkwave`
* Open the vcd file
* Select test_ha and then drop the signals you want to display into the Wave frame.
* You should see something like this:

![gtkwave](doc/gtkwave.png "gtkwave output")
