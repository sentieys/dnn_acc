# VLSI Design and Simulation using Synopsys Design Compiler and Mentor Graphics ModelSim

(mise à jour novembre 2022)

[[_TOC_]]

Ce tutorial a pour objectif l'apprentissage des logiciels **Design
Compiler (Synopsys)** et **ModelSim (Mentor Graphics)** dédiés à la
conception et à la simulation d'ASIC et de FPGA.

Les sources de ce fichier sont accessibles ici : https://gitlab.inria.fr/sentieys/vhdl_course/dc_vsim_tutorial.md

Le logiciel Design Compiler (dc) de Synopsys permet la synthèse logique
grâce aux outils `design_vision` (en mode graphique) et
`dc_shell` (en mode command). Le logiciel ModelSim (`vsim`) permet la
simulation temporelle au niveau RTL ou au niveau
porte, à partir des langages VHDL ou Verilog. Ces deux outils sont la
référence au niveau de l’industrie.

Le tutorial va nous permettre, dans un premier temps, d'appréhender ces
outils en présentant leur fonctionnement. Dans un second, en
nous appuyant sur un exemple concret (un compteur/décompteur 4 bits), nous déroulerons le flot complet : simulation RTL, synthèse logique, simulation niveau portes, ajout de contraintes de synthèse, etc.

## 1. Installation de Design Compiler et de ModelSim

### Clone the source files

```
cd
git clone https://gitlab.inria.fr/sentieys/vhdl_course.git
cd vhdl_course/src
```

Vous devez vous placer en mode `tcsh` (ne pas oublier de le
faire dans chaque nouvelle fenêtre terminal)

```
tcsh		#needed only if you are using bash
source ~/vhdl_course/src/environ.csh
```

Vous pouvez modifier votre fichier `.cshrc` en y ajoutant la commande
`source ~/vhdl_course/src/environ.csh`.
De cette façon, lors de votre prochaine connexion, dc et modelsim seront automatiquement
configurés.

### Note importante

L'utilisation des logiciels de Synopsys impose la mise en place de fichiers de configuration : `.synopsys_dc.setup` et
`.synopsys_vss.setup` dans le répertoire où vous travaillez.

Placez vous **toujours** dans le répertoire `~/vhdl_course/src` avant de lancer les outils de Synopsys où ces deux fichiers sont présents.

Si vous souhaitez travailler dans un autre répertoire, il faudra y copier ces deux fichiers de configurations.

Pour utiliser une bibliothèque fondeur (ou *Design Kit*), le fichier
`.synopsys_dc.setup` configure la bibliothèque  `hcmos9gp`.
Elle correspond à une technologie CMOS 130nm de STMicroelectronics et sera utilisée pour faire la synthèse et la
simulation des composants.

### Organisation des différents fichiers utilisés par dc

- Les fichiers `.v` sont des fichiers source en langage Verilog.
- Les fichiers `.vhd` sont des fichiers source en langage VHDL.
- Les fichiers `.sdf` sont des fichiers contenant les temps extraits après synthèse au niveau porte.
- Les fichiers `.tcl` ou `.do` ou `.con` sont des fichiers script de synthèse.
- Les fichiers `.rpt` ou `.out` sont des fichiers dans lesquels on trouve les informations sur le design à différents moments de la synthèse logique.
- Les fichiers `.log` sont des fichiers contenant toutes les commandes et alias de l'application.
- Les fichiers `.db` sont des sauvegardes dans la base de données du logiciel. C'est également l'extension par défaut de dc.
- Les fichiers `.syn` sont des fichiers intermédiaires générés durant l'analyse d'un circuit.
- Les fichiers `.sim`, `.mra` et `.o` sont des fichiers intermédiaires générés par synopsys et utilisés lors de la phase de simulation.
- Les fichiers `.lis` sont des fichiers contenant le code source ainsi que les warnings et les erreurs générés durant l'analyse.

## 2. Synthèse logique

Ce tutorial s'appuie sur l'exemple d'un compteur/décompteur 4 bits dont
nous allons réaliser l'analyse complète en détaillant toutes les étapes
de la synthèse logique.

### 2.1 Lancement de Design Compiler

Il existe deux manières d'utiliser l'outil de synthèse :
-   la première consiste en l'emploi d'une interface en mode ligne. Pour
    cela on utilise la commande `dc_shell` qui lance l'environnement en mode script;
-   la deuxième consiste en l'emploi d'une interface en mode graphique (gui).
    La commande `design_vision` permet de lancer l'environnement et la
    fenêtre Design Vision apparaît alors à l'écran.

Pour les premières utilisations, il est conseillé d'utiliser l'interface
graphique qui est nettement plus conviviale que l'interface en mode
ligne. Toutes les commandes peuvent être lues au fur
et à mesure de leur exécution dans la fenêtre `Console` en bas de l’écran et sont enregistrées dans le fichier `command.log`.
Ces commandes peuvent ensuite être reproduites dans `dc_shell` pour un gain de temps d'utilisation.

Dans le début de ce tutorial, nous utiliserons l'interface en mode
graphique, mais nous présenterons toutefois quelques
commandes de `dc_shell` importantes.

### 2.2 Synthèse d'un compteur/décompteur 4 bits (non signé)

Le fichier VHDL de départ est :
[counter.vhd](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/src/counter.vhd)

```vhdl
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity counter is
  generic (size :integer :=4);
  port ( reset : in Std_Logic; 
         clk   : in Std_Logic;
	      up    : in Std_Logic;
 	      load  : in Std_Logic;
	      value : in Std_Logic_Vector((size-1) downto 0);
         output: out Std_Logic_Vector((size-1) downto 0) );
end counter;

architecture RTL of counter is
   signal count : unsigned((size-1) downto 0);
begin
   synchrone : process (reset,clk) 
   begin
     if reset='1' then 
	     count<=(others => '0');
     elsif clk'event and clk='1' then
         if load='1' then 
		      count<=unsigned(value);
	      elsif up='1' then 
		      count <= count + 1;
	      else 
		      count <= count - 1;
	      end if ;
     end if ;
    end process ;
    output <= std_logic_vector(count);  
end RTL;
```

#### Chargement et compilation d'un fichier VHDL dans Design Vision.

Pour lancer l'outil exécutez donc dans le répertoire `~/vhdl_course/src` la commande :
```
design_vision
```
La fenêtre de l’outil comme dans la figure suivante doit s’ouvrir. On peut alors analyser le fichier `.vhd` à l'aide du menu `File -> Analyze`. Sélectionnez le fichier `counter.vhd` grace à `ADD` en vérifiant que le modèle VHDL et la bibliothèque `WORK` sont bien choisis et lancez l’analyse `OK`. La syntaxe du fichier est ainsi vérifiée et des fichiers au format intermédiaires sont stockés dans le répertoire de travail `./lib`.

#### Synthèse RTL

La synthèse RTL (`elaborate`) correspond à une synthèse indépendante de la technologie
générant la vue RTL : `File -> Elaborate`.
Sélectionnez la bibliothèque (_library_) `WORK`, puis le  
le circuit (_design_) à synthétiser `COUNTER(RTL)`, précisez
éventuellement la valeur des génériques (ici mettre 4 pour size), puis lancez l'élaboration `OK`.
Le schéma correspondant peut être alors visualisé par le menu
`Schematic -> New Design Schematic View`. Un exemple est donné ci dessous.
Analysez ce schéma en vérifiant principalement les bascules présentes.

_Remarque_ : il est probable que vous n’ayez pas les mêmes résultats en
termes de schéma que sur les figures suivantes. Ceci est normal car le
schéma dépend de la version d’outil utilisé et de la version de
bibliothèque ASIC utilisée.  

![rtl schematic](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/rtl.jpg "rtl schematic")

On peut également utiliser la commande `read` qui est équivalente aux deux étapes de `analyze` et `elaborate` : `File -> Read -> counter.vhd`. C'est en pratique plus simple.

#### Contrainte sur la fréquence d’horloge

Avant de faire la synthèse du circuit, il est nécessaire de lui indiquer
une fréquence d’horloge. Celle-ci servira comme contrainte pour la
synthèse. Pour cela, le plus simple est d’exécuter la commande suivante dans la fenêtre de commande
en bas de l’outil :
```
create_clock -period 10.0 clk
```
Ici une période de 10ns est donnée à l’horloge nommée clk.

#### Optimisation du circuit

L'étape d'optimisation permet de passer au niveau porte en utilisant pour cela
des portes logiques de la bibliothèque de la technologie ciblée : `Design -> Compile Design`.

Ouvrez ensuite le nouveau schéma (voir figure ci dessous).   
On observe le résultat suivant qui montre que le design est maintenant
une connexion de portes de la bibliothèque `hcmos9gp`. Pour voir le
nom des cellules de la bibliothèque, faites un clic droit sur une
cellule (par exemple une bascule) et vous trouverez le nom (ref name) de
la cellule dans les _properties_ (exemple FD2QLL pour une bascule).

![gate-level schematic](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/gate.jpg "gate-level schematic")

La documentation pdf décrivant la bibliothèque de cellules est accessible à
`/usr/local/DesignKit/hcmos9gp_920/CORE9GPLL_SNPS_AVT_4.1/doc/databook_1.2V/CORE9GPLL_1.2V_databook.pdf`.
En y cherchant la description des cellules utilisées dans votre schéma,
vous pourrez ainsi bien comprendre le rôle de chaque cellule de la bibliothèque `hcmos9gp`. Les chiffres
indiqués sur la documentation de la cellule vous font également le lien
avec les TD de micro-electronique (surface de la cellule, valeurs des
capacités, etc.). Regardez la documentation pour quelques cellules qui
apparaissent dans votre schéma.

Il est maintenant nécessaire de sauvegarder le résultat de synthèse en
vue d'une simulation de niveau porte ultérieure. Pour cela exécutez les commandes suivantes dans la ligne de commande
`design_vision>` :
```
change_names -hierarchy -rule vhdl
```
permet de préparer la _netlist_ pour un export en VHDL.
```
write_sdf counter_gate.sdf
```
permet de sauvegarder les paramètres temporels de simulation au format sdf (standard delay format) qui serviront pour une simulation au niveau porte avec temps de propagation des portes.
Sauvegardez votre compteur optimisé `File -> Save -> counter_gate.db`.
Sauvegardez votre compteur optimisé `File -> Save As`. Choisissez le format `VHDL`, sélectionnez `Save All Design in Hierarchy` et appelez
votre fichier `counter_gate.vhd`. Vous pouvez éditer ce fichier et constater que c’est un VHDL structurel qui connecte les cellules de la bibliothèque entre elles et qui est équivalent au schéma.

#### Appliquer des contraintes d'optimisation

Les contraintes d'optimisation sont des déclarations explicites qui définissent les objectifs à atteindre pour le circuit considéré. SYNOPSYS propose des contraintes de temps, de surface et d’énergie. Par défaut, l’outil minimisera la surface en respectant une éventuelle contrainte de temps.
Lors de la phase d'optimisation, Design Compiler utilise deux modèles de contraintes:
- les contraintes implicites, qui sont imposées par la bibliothèque technologique et
- les contraintes d'optimisation (ou contraintes explicites) imposées par l'utilisateur.

Il existe trois façons différentes de spécifier des contraintes:
- Ecriture de ligne de commande dans `dc_shell>` ou appel d’un script par la commande `source`. C’est le moyen le plus efficace et donc celui à privilégier. Quelques exemples de contraintes sont donnés ci dessous (contenu du fichier `counter.con`). Il n'est pas demander de les appliquer.
```
# Create user defined variables
designer = "Olivier Sentieys"
company = "ENSSAT"
# Time Budget
create_clock -period 10.0 -waveform {0 5} \[get_ports clk\]
set_dont_touch_network clk
set_clock_uncertainty -setup 0.2 \[get_ports clk\]
set_input_delay -max 1.0 -clock clk \[get_ports "up load value"\]
set_output_delay -max 0.5 -clock clk \[get_ports output\]
# Area Constraint
set_max_area 5000
# Operating Environment
set_wire_load_model -name area_18Kto24K
set_driving_cell -library CORE9GPLL -lib_cell FD2QNLL -pin QN \[get_ports "up load value"\]
set_load \[load_of CORE9GPLL/FD2QNLL/D\] \[get_ports output\]
report_constraints
```
- Sélection des menus de contraintes dans la fenêtre Design Vision.
    - `Attributes -> Optimization Constraints -> Design Constraint` pour spécifier les contraintes en terme de surface (`Area`), de puissance (`Power`) ou encore de sortance (`Fanout`).
    - `Attributes -> Optimization Constraints -> Timing Constraint` pour spécifier les contraintes en terme de temps de traversée entre une entrée et une sortie ou de fréquence d'horloge.
- Ecriture de fichiers source `.vhd` contenant des contraintes sous la forme d'`attribute` (cf. exemple de code ci dessous).
```vhdl
library IEEE;  
use IEEE.STD_LOGIC_1164.ALL;  
library SYNOPSYS;  
use SYNOPSYS.ATTRIBUTES.ALL;  

entity counter is  
  port ( reset  : in Std_Logic;  
         clk    : in Std_Logic;  
         up     : in Std_Logic;  
         load   : in Std_Logic;  
         val    : in integer range 0 to 15;  
         sortie : out integer range 0 to 15) ;  
         attribute MAX_AREA of counter : entity is 70.0;  
         attribute MAX_DELAY of sortie : signal is 2.0;  
end counter;
```

#### Analyse du résultat de synthèse du circuit

Une fois synthétisé, le circuit peut être analysé grâce aux différents rapports (`report`) :
- surface : `Design -> Report Area` dans les menus ou par la commandes `report_area`. La surface est en micro-mètre carré ($\mu^2$).
- consommation : `Design -> Report Power` ou `report_power`.
- fréquence maximale (chemin critique) : `Timing -> Report Timing Path` ou `report_timing`.

Le résultat du chemin critique pour le compteur 4 bits est donné ci dessous.
```
 Point                                    Incr       Path
  -----------------------------------------------------------
  clock clk (rise edge)                    0.00       0.00
  clock network delay (ideal)              0.00       0.00
  count_reg[0]/CP (FD2QLL)                 0.00       0.00 r
  count_reg[0]/Q (FD2QLL)                  0.14       0.14 f
  U36/Z (IVLLX05)                          0.16       0.30 r
  U25/Z (AO2LL)                            0.18       0.48 f
  U34/Z (AO20NLL)                          0.19       0.67 f
  U32/Z (AO8ALL)                           0.07       0.74 r
  U31/Z (AO6NLL)                           0.12       0.86 r
  count_reg[2]/D (FD2QLL)                  0.00       0.86 r
  data arrival time                                   0.86

  clock clk (rise edge)                   10.00      10.00
  clock network delay (ideal)              0.00      10.00
  count_reg[2]/CP (FD2QLL)                 0.00      10.00 r
  library setup time                      -0.19       9.81
  data required time                                  9.81
  -----------------------------------------------------------
  data required time                                  9.81
  data arrival time                                  -0.86
  -----------------------------------------------------------
  slack (MET)                                         8.95
```

Dans cet exemple, le chemin critique est 0.86ns auquel il faut ajouter le temps de setup (ici 0.19ns) pour obtenir la fréquence maximale (soit 952MHz). Comme une horloge de 10ns (100MHz) avait été spécifiée, le `slack` est donc la marge de temps restante.
Le détail donne l’accumulation des temps de propagation à travers les différentes portes (par exemple IVLLX05 est un inverseur dont le temps de propagation est de 0.16ns). Les `r` et `f` sont pou _rising_ et _falling_ au niveau des temps de propagation des portes.
En cliquant sur les nœuds en bleu, vous pouvez les afficher dans le schéma porte.
- `Timing -> Timing Analysis Driver` permet d’obtenir différents chemins critiques (ici 4). Lorsque vous cliquez sur un des chemins, celui-ci doit s’afficher dans le schéma porte comme sur la figure suivante. Vérifiez que les chemins critiques sont bien entre deux bascules et faire le rapprochement avec le calcul de la fréquence maximale.
- `Timing-> Path Slack` permet de visualiser un histogramme des différents chemins. On voit ainsi si il y a beaucoup de chemins qui limitent la fréquence maximale, ou si il y en a peu et qu’en les optimisant on gagnera en fréquence max.

![critical path](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/critical_path.png "critical path")

Ces rapports peuvent également être sauvés dans des fichiers texte.
```
report_area > rep_area_counter.txt
```

#### Utilisation d’un script `dc_shell` de synthèse

Les étapes lancées précédemment à partir de l’environnement graphique `design_vision` peuvent être réalisées à partir d’un script. Cela fait
gagner du temps et permet de fournir à d’autres utilisateurs des fichiers qui automatisent la génération du circuit.  
Un fichier [`counter.tcl`](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/src/counter.tcl) vous est fourni et contient les instructions nécessaires. Il réalise automatiquement les étapes précédentes.

```
read_vhdl counter.vhd
current_design counter
link
write -h -f vhdl -o counter_rtl.vhd
list_designs
list_libs
create_clock -period 10.0 -waveform {0 5} [get_ports clk]
compile_ultra
report_constraint -all
report_timing
report_area
report_power
change_name -hierarchy -rule vhdl
write_sdf counter_gate.sdf
write -hierarchy -format vhdl -output counter_gate.vhd
```

La commande `source counter.tcl` peut soit être lancé dans la fenêtre script `design_vision>` soit dans un `dc_shell`.
Pour bien comprendre ce que fait chaque commande dans ce script, nous vous conseillons d’exécuter chaque commande une par une dans Design Vision. Ensuite, vous pourrez utiliser ce type de script pour gagner du temps pour compiler vos designs pendant le projet. Le gain de temps est réellement appréciable par rapport à l’utilisation de l’environnement graphique en mode _cliquodrôme_.

## 3. Simulation logique avec ModelSim de Mentor Graphics

### 3.1. Lancement et configuration des outils

Maintenant que le design est prêt, il faut valider son fonctionnement par des simulations. L'invocation de l’outil de simulation ModelSim
se fait par la commande `vsim`.
ModelSim fonctionne autour d'un projet associé à une bibliothèque dans lequel vous mettrez tous vos fichiers VHDL à simuler. Ci dessous les étapes à suivre :
-   `File->Change Directory` et choisir le répertoire courant `~/vhdl_course/src` (si nécessaire);
-   `File->New->Project` et donner par exemple le nom `counter` à ce projet dans le répertoire courant;
-   Faire le lien avec la bibliothèque de description des composants niveau porte par la commande `vmap CORE9GPLL /usr/local/DesignKit/hcmos9gp_920/CORE9GPLL_SNPS_AVT_4.1/MENTOR_MODELSIM` ou **`do initlib.do`** qui contient cette même commande.

Comme vous le savez déjà, pour simuler un circuit, il faut plusieurs descriptions VHDL : un fichier source décrivant le circuit, un fichier de simulation (_testbench_), un fichier de configuration.
Nous utiliserons le même exemple de fichier source : [counter.vhd](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/src/counter.vhd).

```vhdl
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter is
  port (  reset : in Std_Logic;
          clk   : in Std_Logic;
	      up    : in Std_Logic;
 	      load  : in Std_Logic;
	      value : in Std_Logic_Vector((size-1) downto 0);
          output: out Std_Logic_Vector((size-1) downto 0) );
end counter;

architecture RTL of counter is
...
end RTL;
```

Le fichier de simulation (_testbench_) est [sim_counter.vhd](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/src/sim_counter.vhd).

```vhdl
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY bench IS
END bench;
ARCHITECTURE test OF bench IS
  COMPONENT counter
    generic (size :integer :=4);
    port (  reset : in Std_Logic;
            clk   : in Std_Logic;
            up    : in Std_Logic;
            load  : in Std_Logic;
            value : in Std_Logic_Vector(3 downto 0);
            output: out Std_Logic_Vector(3 downto 0) );
  end COMPONENT;
  constant Tclk : time := 10 ns;
  signal clk_cpt : std_logic:= '0';
  signal reset_cpt: std_logic:= '1';
  signal up_cpt  : std_logic:= '1';
  signal load_cpt: std_logic:= '0';
  signal val_cpt : Std_Logic_Vector(3 downto 0);
  signal out_cpt : Std_Logic_Vector(3 downto 0);
begin
  cpt : counter generic map(4) port map (reset_cpt, clk_cpt, up_cpt, load_cpt, val_cpt,out_cpt);

  GENERATE_CLOCK : process
  begin
	clk_cpt<= '0';
	WAIT FOR Tclk/2;
	clk_cpt<= '1';
	WAIT FOR Tclk/2;
  end process GENERATE_CLOCK;

  CPT_SIM : process
  begin
	reset_cpt<= '1';
	up_cpt<= '1';
	load_cpt<= '0';
	val_cpt <= "1001";
	WAIT FOR 3*Tclk;
	reset_cpt<= '0';
	WAIT FOR 10*Tclk;
	load_cpt<= '1';
	WAIT FOR 10*Tclk;
	load_cpt<= '0';
	WAIT FOR 30*Tclk;
	up_cpt<= '0';
	WAIT FOR 40*Tclk;
	wait;
  end process CPT_SIM;
end test;
```

Le fichier de configuration permet d'indiquer au simulateur quel architecture il doit simuler (ici `comportementale`) :
[cfg_counter.vhd](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/src/cfg_counter.vhd).

```vhdl
library IEEE;  
use IEEE.STD_LOGIC_1164.ALL;  
library work;  
use work.ALL;    
configuration counter_cfg of bench is  
  for test  
    for cpt:counter
      use entity work.counter(RTL);  
    end for;  
  end for;  
end counter_cfg;
```

La traduction de ce fichier de configuration est : pour l'instance `cpt` du composant `counter` dans le testbench, utiliser l'architecture `RTL` du composant `counter` dans la bibliothèque `work`. Ceci correspond à une simulation au niveau RTL. Pour une simulation après synthèse au niveau porte. Il suffira de modifier le nom de l'architecture dans le fichier de configuration. En effet la _netlist_ générée par Synopsys lors de la synthèse a pour nom `SYN_RTL`. La configuration au niveau gate (voir fichier `cfg_counter_gate.vhd`) deviendra donc :
```vhdl
configuration counter_cfg_gate of bench_gate is  
  for test  
    for cpt:counter
      use entity work.counter(SYN_RTL);  
     end for;  
  end for;  
end counter_cfg_gate;
```

### 3.2. Analyse des fichiers

Avant de simuler le circuit, il faut ajouter dans le projet les fichiers VHDL puis les compiler.
-   `File->Add to project->counter.vhd`
-   `File->Add to project->sim_counter.vhd`
-   `File->Add to project->cfg_counter.vhd`
-   Précisez l’ordre de compilation des fichiers grâce à la commande `Compile->Compile Order`, en mettant `counter.vhd` en premier, suivi de `sim_counter.vhd` puis de `cfg_counter.vhd`.
-   `Project->Compile All`
Il est bien sur possible de compiler les fichiers un par un en cas d'erreur. Un éditeur de texte s'ouvre lorsque l'on double-clique sur les
fichiers.

### 3.3. Lancement de la simulation au niveau RTL

Choisissez l'onglet `Library`, puis, en double-cliquant sur la configuration `counter_cfg`, la simulation se charge.  
Ceci est équivalent à : `Simulate->Start Simulation->work.counter_cfg`
-   Dans la fenêtre `Objects` sélectionnez l'ensemble des signaux et les _drag and drop_ dans la fenêtre `Wave`.
-   `Simulate -> Run->Run 100ns` permet de faire avancer la simulation de 100ns (ou utilisez les icônes).
-   Répéter cette opération tant que nécessaire... Vous devez voir les sorties de votre compteur s’incrémenter.

Vérifiez avec précision si le design se comporte comme souhaité (reset asynchrone, chargement synchrone, comptage, décomptage).
La figure ci dessous montre la simulation du compteur après 200ns.

![RTL simulation](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/sim_rtl2.jpg "RTL simulation")

### 3.4. Simulation au niveau porte

Maintenant que vous venez de faire la simulation au niveau *RTL*, il faut vérifier le résultat de la synthèse par simulation au niveau porte avec la netlist `counter_gate.vhd`. Pour cela, le plus simple (mais ce n'est pas obligatoire) est de créer un nouveau projet.
-   `File->New->Project` et donner par exemple le nom `sim_gate` à ce projet dans le répertoire courant.
-   Changez également le nom de la bibliothèque de `work` vers par exemple `work_gate`.
-   `File->Add to project->counter_gate.vhd`
-   `File->Add to project->sim_counter_gate.vhd`
-   `File->Add to project->cfg_counter_gate.vhd`
Recompilez l'ensemble. La configuration instancie maintenant la netlist au niveau porte et non plus la spécification RTL d'origine du compteur.
Rappel : il est en outre nécessaire d’avoir généré un fichier au format sdf `counter_gate.sdf` sous Design Vision.
-   `Simulate -> Start Simulation`
-   Dans l'onglet `Design`, chargez la configuration du test `counter_cfg_gate` en choisissant `ps` (pico-second) comme résolution du simulateur.
-   Dans l'onglet `SDF`, chargez (bouton `Add...`) le fichier sdf `counter_gate.sdf` associé au composant à tester `bench_gate/cpt`.
    -   `Browse->counter_gate.sdf`
    -   `Apply to Region->bench_gate/cpt` (on applique les temps seulement au composant `cpt` de l’entité `bench_gate`).
    -   Cocher l’option `reduce errors to warnings`.
-   Dans l’onglet des librairies, ajoutez `CORE9GPLL`
Visualisez, comme au niveau RTL, les signaux d’entrée et de sortie du compteur et lancez la simulation.   

La simulation prend maintenant en compte les portes logiques qui constituent la _netlist_ du compteur et les informations de délais
fournies par le fichier sdf.
En zoomant sur un front d’horloge vous voyez maintenant apparaître des temps de propagation entre l'horloge et les sorties du compteur.

![gate-level simulation](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/sim_gate.jpg "gate-level simulation")

## 4. Compteur 32 bits

**IMPORTANT** : recompiler et simulez avec une taille de 32 bits pour le compteur afin de mieux vous rendre compte des problèmes contraintes de fréquence d’horloge, de chemins critiques, de temps de propagation et de _glitchs_. Avec l'utilisation du script de synthèse cela ne vous prendra que quelques minutes pour relancer la simulation niveau portes du compteur 32 bits. Il vous faudra modifier également le fichier source et le testbench.

Visualisez le schéma niveau porte, le chemin critique, la surface et relancez enfin la simulation niveau porte.
