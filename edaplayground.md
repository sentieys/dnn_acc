# Simulation with EDA Playground

[EDA Playground](https://www.edaplayground.com) gives engineers immediate hands-on exposure to simulating and synthesizing SystemVerilog, Verilog, VHDL, C++/SystemC, and other HDLs. All you need is a web browser.

## Quick Start

- [Log in](http://www.edaplayground.com/login) to EDA Playground. 
- Click the Log in button (top right) and the register by clicking on 'Register for a full account'.
- Select your language from the `Testbench + Design` menu.
- Select your simulator from the `Tools & Simulators` menu. 
- Type in your code in the testbench and design windows.
- Click Run.

[Tutorial](http://eda-playground.readthedocs.io/en/latest/tutorial.html)

## Simulate the Half Adder

- [Log in](http://www.edaplayground.com/login) to EDA Playground. 
- Select VHDL from the `Testbench + Design` menu.
- Enter HalfAdder in the `Top entity` form.
- Select 'Mentor Questa' from the `Tools & Simulators` menu. 
- Check the `Use run.do Tcl file` and `Open EPWave after run if not` box on.
- In the `Run Time:` form enter `40 ns`.

- In the 'design.vhd' window, copy/paste the content of
[HalfAdder.vhdl](VHDL/HalfAdder.vhdl)

- In the testbench window, create a run.do file, abd the copy/paste the content of
[sim_HalfAdder.do](VHDL/sim_HalfAdder.do)
- Remove the content of 'testbench.vhd'

You should have something like this.

![edapg1](doc/edapg1.png "eda playground configured")


- `Save` your design,
- and then `Run` your design.

You should see the result of your simulation.

![edapg2](doc/edapg2.png "eda playground after running simulation")


