library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity dec_55_154 is
  port( count: in integer range 0 to 255;
        decode: out std_logic );
end dec_55_154;

architecture RTL OF dec_55_154 is
begin
   ...
end RTL;
