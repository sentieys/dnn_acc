library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity C1 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C1;

architecture RTL OF C1 is
begin
process (A,B) begin
  if (A=B) then 
    Egal <= '1'; Sup <= '0'; Inf <= '0';
  elsif (A>B) then 
    Egal <= '0'; Sup <= '1'; Inf <= '0';
  elsif (A<B) then 
    Egal <= '0'; Sup <= '0'; Inf <= '1';
  end if;
end process;
end RTL;

entity C2 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C2;

architecture RTL OF C2 is
begin
process (A,B) begin
  if (A=B) then 
    Egal <= '1'; Sup <= '0'; Inf <= '0';
  elsif (A>B) then 
    Sup <= '1'; 
  else  
    Inf <= '1';
  end if;
end process;
end RTL;

entity C3 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C3;

architecture RTL OF C3 is
begin
process (A,B) begin
  if (A=B) then 
   Egal <= '1'; 
  elsif (A>B) then 
   Sup <= '1'; 
  else
   Inf <= '1';
  end if;
end process;
end RTL;

entity C4 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C4;

architecture RTL OF C4 is
begin
process (A,B) begin
  if (A=B) then 
    Egal <= '1'; Sup <= '0'; Inf <= '0';
  elsif (A>B) then 
    Egal <= '0'; Sup <= '1'; Inf <= '0';
  elsif (A<B) then 
    Egal <= '0'; Sup <= '0'; Inf <= '1';
  else
    Egal <= '0'; Sup <= '0'; Inf <= '0';
  end if;
end process;
end RTL;

entity C5 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C5;

architecture RTL OF C5 is
begin
    Egal <= '1' when A=B else '0';
    Sup <= '1' when A>B else '0';
    Inf <= '1' when A<B else '0';
end RTL;

entity C6 is
  port( A,B: in  integer range 0 to 255;
        egal, sup, inf:   out std_logic );
end C6;

architecture RTL OF C6 is
begin
process (A,B) begin
  if (A=B) then 
    Egal <= '1'; Sup <= '0'; Inf <= '0';
  elsif (A>B) then 
    Egal <= '0'; Sup <= '1'; Inf <= '0';
  else
    Egal <= '0'; Sup <= '0'; Inf <= '1';
  end if;
end process;
end RTL;
