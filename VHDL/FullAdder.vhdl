library IEEE;
use IEEE.std_logic_1164.all;
entity FullAdder is
port (a, b, c_in: in std_ulogic;
      sum, carry: out std_ulogic );
end entity FullAdder;

architecture dataflow of FullAdder is
	signal s1, s2, s3 : std_ulogic;
	constant gate_delay: time:= 5 ns;
begin
	L1: s1 <= (a xor b) after gate_delay;
	L2: s2 <= (c_in and s1) after gate_delay;
	L3: s3 <= (a and b) after gate_delay;
	L4: sum <= (s1 xor c_in) after gate_delay;
	L5: carry <= (s2 or s3) after gate_delay;
end architecture dataflow;


