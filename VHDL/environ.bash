export LM_LICENSE_FILE=5280@mentor.cnfm.fr

echo Configuring ModelSimSE SE-64 10.6c
export MODEL_TECH=/usr/local/Modelsim/modeltech/bin
export PATH=/usr/local/Modelsim/modeltech/bin:$PATH

echo Configuring Catapult2023.1
export MGC_HOME=/usr/local/Siemens_EDA/Catapult_Synthesis_2023.1-1033555/Mgc_home
export PATH=/usr/local/Siemens_EDA/Catapult_Synthesis_2023.1-1033555/Mgc_home/bin:$PATH

#echo Configuring Design Compiler 2010.03
#export LM_LICENSE_FILE=$LM_LICENSE_FILE:27000@synopsys.cnfm.fr
#export SYNOPSYS=/usr/local/DesignVision-2010.03-SP4
#export PATH=$SYNOPSYS/amd64/syn/bin:$SYNOPSYS:$PATH

export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu
export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu