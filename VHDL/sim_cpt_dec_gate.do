vmap CORE9GPLL /usr/local/DesignKit/hcmos9gp_920/CORE9GPLL_SNPS_AVT_4.1/MENTOR_MODELSIM

vcom cpt_dec_gate.vhd
vcom sim_cpt_dec.vhd

vsim -L CORE9GPLL -sdftyp bench_cpt_dec/cpt=./cpt_dec_gate.sdf -sdfnoerror -t 100fs work.cpt_dec_gate_cfg

view structure
view signals
view wave

add wave -noupdate clk
add wave -noupdate reset
add wave -noupdate dout
add wave -noupdate decode

run 520 ns
