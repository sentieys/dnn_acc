read_vhdl counter.vhd
current_design counter
link
write -h -f vhdl -o counter_rtl.vhd
list_designs
list_libs
create_clock -period 10.0 -waveform {0 5} [get_ports clk]
compile_ultra
report_constraint -all
report_timing
report_area
report_power
change_name -hierarchy -rule vhdl
write_sdf counter_gate.sdf
write -hierarchy -format vhdl -output counter_gate.vhd


