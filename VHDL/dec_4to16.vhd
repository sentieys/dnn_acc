library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity dec_4to16 is
  port( Din: in std_logic_vector(3 downto 0);
        CS: in std_logic;
        Dout: out std_logic_vector(15 downto 0) );
end dec_4to16;

architecture RTL OF dec_4to16 is
begin
   ...
end RTL;
