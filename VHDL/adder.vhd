library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity ADDER is
  generic (N : positive := 8);
  port( A,B: in  std_logic_vector(N-1 downto 0);
        C:   out std_logic_vector(N-1 downto 0) );
end ADDER;

architecture RTL OF ADDER is
   signal A_S,B_S,C_S : SIGNED(N-1 downto 0) :=  (others => '0');
begin
   A_S <= SIGNED(A);
   B_S <= SIGNED(B);
   C_S <= A_S + B_S;
   C <= STD_LOGIC_VECTOR(C_S);
end RTL;
