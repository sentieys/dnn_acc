library IEEE;
use IEEE.std_logic_1164.all;
entity FullAdder is
port (a, c_in, b: in std_logic;
      sum, carry: out std_logic);
end entity FullAdder;

architecture behavioral of FullAdder is

  constant delay: time:= 5 ns;
begin

HA1: process () is
begin
  s1 <= (a xor b) after delay;
  s3 <= (a and b) after delay;
end process HA1;

HA2: process() is
begin
  sum <= (s1 xor c_in) after delay;
  s2 <= (s1 and c_in) after delay;
end process HA2;

OR1: process () 
begin
  carry <= (s2 or s3) after delay;
end process OR1;

end behavioral;

