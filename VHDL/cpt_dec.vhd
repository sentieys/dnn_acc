library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cpt_dec is
  
  port ( reset: in std_logic; 
         clk: in std_logic;
         decode: out std_logic;
         dout: out std_logic_vector(7 downto 0));
end cpt_dec;

architecture RTL of cpt_dec is
  signal count: integer range 0 to 255;
begin
   cpt: process (reset,clk) 
   begin
     if reset='0' then 
	     count <= 0;
     elsif clk'event and clk='1' then
         if count = 255 then 
		      count <= 0;
	      else 
		      count <= count + 1;
	      end if ;
     end if ;
    end process ;
    dec: process(count)
    begin
      for i in 0 to 7 loop
        if i=count then
          dout(i) <= '1';
        else
          dout(i) <= '0';
        end if;
      end loop;
    end process;
    decode <= '1' when (count >= 55 and count < 155) else '0';
end RTL;
  
