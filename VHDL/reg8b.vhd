   
   library IEEE;
   use IEEE.STD_LOGIC_1164.all;
   entity REG8B is
   port (   data: inout STD_LOGIC_VECTOR(7 downto 0);
            clk, en, wb: in  STD_LOGIC );
   end REG8B;

   architecture RTL of REG8B is
     signal REG : STD_LOGIC_VECTOR(7 downto 0);
   begin
        ...
   end RTL;

  