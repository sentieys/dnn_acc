library IEEE;
use IEEE.std_logic_1164.all;

-- tesbench of FA

entity Test_FA is 
end Test_FA;

architecture test of Test_FA  is

   component FullAdder 
      port (a, b, c_in: in std_logic;
            sum, carry: out std_logic);
   end component;

   signal SA, SB, SSum, SCin , SCout: std_logic := '0';

   constant cycle: time := 10 ns;

   -- configuration
   for U1:FullAdder use entity work.FullAdder(behavioral);

begin

   -- Component Instantiation
   U1: FullAdder port map(a=>SA, b=>SB, sum=>SSum, c_in=>SCin, carry=>SCout);	

   Simulation: process 
   begin

...
	wait;

   end process;
end test;

