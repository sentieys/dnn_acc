vsim FullAdder(dataflow)
view structure
view signals
view wave
add wave -noupdate -divider -height 32 Inputs
add wave -noupdate a
add wave -noupdate b
add wave -noupdate c_in
add wave -noupdate -divider -height 32 Outputs
add wave -noupdate sum
add wave -noupdate carry
force a 0 0
force b 0 0
force c_in 0 0
# a = 1, b = 0 at 10 ns
force a 1 10
# a = 0, b = 1 at 20 ns
force a 0 20
force b 1 20
# a = 1, b = 1 at 30 ns
force a 1 30
# a = 1, b = 1, c_in = 1 at 40 ns
force c_in 1 40
run 50
