library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity park is
 port (reset,clk,PA,PB: in std_logic; 
       nb_cars: buffer integer range 0 to 100;
       full: out std_logic);
end park;

architecture RTL of park is

   type states is (idle, E00, E01, E02, E03, E10, E11, E12, E13);
   signal current_state, next_state: states;
   signal up,down: std_logic;

begin

FULL <= '1' when NB_CARS=100 else '0'; 

counter: process(reset, clk)
begin
   if reset='0' then
      NB_CARS <= 0;
   elsif (clk'event and clk='1') then
      if up='1' and NB_CARS /= 100 then 
	      NB_CARS <= NB_CARS + 1;
      elsif down='1' and NB_CARS /= 0 then 
	      NB_CARS <= NB_CARS - 1;
      end if; 
  end if;
end process; 


StateRegister: process(reset, clk)
begin
   if reset='0' then
      current_state <= idle;
  elsif (clk'event and clk='1') then
      current_state <= next_state; 
  end if;
end process; 

moore:process(PA,PB,current_state)
begin  
   case current_state is  
      when idle =>
	      if PA = '1' then
 	         next_state <= E00; --leaving
	      elsif PB = '1' then 
 	         next_state <= E10; --entering
	      else
 	         next_state <= idle;
	      end if;

      when E00 => --leaving
         if PB = '1' then 
 	         next_state <= ...;
	      else
 	         next_state <= ...;
	      end if;
 ...

      when E10 => --entering
         if PA = '1' then 
 	         next_state <= ...;
	      else
 	         next_state <= ...;
	      end if;
 ...

	   when others =>
	      ... 
    end case;
end process;

up <= '1' when ...;
down <= '1' when ...;

end RTL;