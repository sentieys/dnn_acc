library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work; 
use work.all;

ENTITY bench IS
END bench; 

ARCHITECTURE test OF bench IS
COMPONENT counter 
  generic (size :integer :=4);
  port (  reset : in Std_Logic; 
          clk   : in Std_Logic;
    up    : in Std_Logic;
     load  : in Std_Logic;
    value : in Std_Logic_Vector(3 downto 0);
    output: out Std_Logic_Vector(3 downto 0) );
end COMPONENT;

constant Tclk : time := 10 ns;
signal clk_cpt : std_logic:= '0';
signal reset_cpt: std_logic:= '1';
signal up_cpt  : std_logic:= '1';
signal load_cpt: std_logic:= '0';
signal val_cpt : Std_Logic_Vector(3 downto 0);
signal out_cpt : Std_Logic_Vector(3 downto 0);

begin

cpt : counter generic map(4) port map (reset_cpt, clk_cpt, up_cpt, load_cpt, val_cpt,out_cpt);


GENERATE_CLOCK : process
  begin
	clk_cpt<= '0';
	WAIT FOR Tclk/2;
	clk_cpt<= '1';
	WAIT FOR Tclk/2;
  end process GENERATE_CLOCK;

CPT_SIM : process
  begin
	reset_cpt<= '1';
	up_cpt<= '1';
	load_cpt<= '0'; 
	val_cpt <= "1001";
	WAIT FOR 3*Tclk;
	reset_cpt<= '0';
	WAIT FOR 10*Tclk;
	load_cpt<= '1';
	WAIT FOR 10*Tclk;
	load_cpt<= '0';
	WAIT FOR 30*Tclk;
	up_cpt<= '0';
	WAIT FOR 40*Tclk;
	
	wait;
  end process CPT_SIM;


end test;
