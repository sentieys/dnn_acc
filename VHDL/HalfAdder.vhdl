library IEEE;
use IEEE.std_logic_1164.all;

entity HalfAdder is
port (a, b : in std_ulogic;
sum, carry :out std_ulogic);
end HalfAdder;

architecture behavioral of HalfAdder is
begin
	sum <= (a xor b) after 5 ns;
	carry <= (a and b) after 5 ns;
end behavioral;

