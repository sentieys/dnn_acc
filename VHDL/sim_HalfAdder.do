# Load the HalfAdder for simulation
vsim HalfAdder
# Open some selected windows for viewing view structure
view signals
view wave
# Show some of the signals in the wave window 
add wave -noupdate -divider -height 32 Inputs 
add wave -noupdate a
add wave -noupdate b
add wave -noupdate -divider -height 32 Outputs 
add wave -noupdate sum
add wave -noupdate carry
# Set some test paBerns
#a=0,b=0 at 0ns
force a 0 0
force b 0 0
#a=1,b=0 at 10ns
force a 1 10
#a=0,b=1 at 20ns
force a 0 20
force b 1 20
#a=1,b=1 at 30ns
force a 1 30
#end
force a 0 40
force b 0 40

vcd file dump.vcd
vcd add -r sim:/*
run -all
exit
