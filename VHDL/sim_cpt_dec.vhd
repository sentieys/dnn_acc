library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work; 
use work.all;

ENTITY bench_cpt_dec IS
END bench_cpt_dec; 

ARCHITECTURE test OF bench_cpt_dec IS
COMPONENT cpt_dec 
  port ( reset: in std_logic; 
         clk: in std_logic;
         decode: out std_logic;
         dout: out std_logic_vector(7 downto 0));
end COMPONENT;

constant Tclk: time := 2 ns;
signal clk: std_logic:= '0';
signal reset: std_logic:= '0';
signal dout: std_logic_vector(7 downto 0) := (others=>'0');
signal decode: std_logic := '0';

begin

cpt : cpt_dec port map (reset, clk, decode, dout);

clock: process
  begin
	clk<= '0';
	WAIT FOR Tclk/2;
	clk<= '1';
	WAIT FOR Tclk/2;
  end process clock;

rstb: process
  begin
    reset <= '0';
	  wait for Tclk;
    reset <= '1';
    wait;
  end process;

end test;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.ALL;

configuration cpt_dec_gate_cfg of bench_cpt_dec is
	for test
		for cpt:cpt_dec
			use entity work.cpt_dec(SYN_RTL);
		end for;
	end for;
end cpt_dec_gate_cfg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.ALL;

configuration cpt_dec_cfg of bench_cpt_dec is
	for test
		for cpt:cpt_dec
			use entity work.cpt_dec(RTL);
		end for;
	end for;
end cpt_dec_cfg;
