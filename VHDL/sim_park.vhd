library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work; 
use work.all;

entity sim_park is
end sim_park;

architecture test_park of sim_park is

COMPONENT park 
	port (reset,clk,PA,PB: in std_logic; 
       nb_cars: buffer integer range 0 to 100;
       full: out std_logic);
end COMPONENT;

constant Tclk : time := 10 ns;
constant TPAPB : time := 5*Tclk;
signal clk: 	std_logic:= '0';
signal reset: 	std_logic:= '1';
signal full: 	std_logic:= '0';
signal PA,PB:	std_logic:= '0';
signal nb_cars: integer range 0 to 100;

begin

sys : park port map (reset, clk, PA, PB, nb_cars, full);

GENERATE_CLOCK : process
  begin
	clk<= '0';
	WAIT FOR Tclk/2;
	clk<= '1';
	WAIT FOR Tclk/2;
  end process GENERATE_CLOCK;

PARK_SIM : process
  begin
	reset <= '0';
	PA <= '0';
	PB <= '0'; 
	WAIT FOR 3*Tclk;
	reset <= '1';

	--entering first car
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR TPAPB;
	PB <= '0';
	WAIT FOR Tclk;
	PA <= '0';
	WAIT FOR TPAPB;

	--entering next car
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR TPAPB;
	PB <= '0';
	WAIT FOR Tclk;
	PA <= '0';
	WAIT FOR TPAPB;

	--entering next car
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR TPAPB;
	PB <= '0';
	WAIT FOR Tclk;
	PA <= '0';
	WAIT FOR TPAPB;

--leaving one car
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR TPAPB;
	PA <= '0';
	WAIT FOR Tclk;
	PB <= '0';
	WAIT FOR TPAPB;

--leaving one car
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR TPAPB;
	PA <= '0';
	WAIT FOR Tclk;
	PB <= '0';
	WAIT FOR TPAPB;

--entering next car
	WAIT FOR Tclk;
	PB <= '1';
	WAIT FOR Tclk;
	PA <= '1';
	WAIT FOR TPAPB;
	PB <= '0';
	WAIT FOR Tclk;
	PA <= '0';
	WAIT FOR TPAPB;

	WAIT;
  end process PARK_SIM;

end test_park;