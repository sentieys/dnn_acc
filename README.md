# Repository for Hardware Accelerators for Deep Neural Networks (DNN_ACC) course (O. Sentieys) - Master SE (M2)

## Installation
To download the files used during the DNN_ACC course:
```
cd
git clone https://gitlab.inria.fr/sentieys/dnn_acc.git
cd dnn_acc
```

## GEMM Code Exploration
Different code versions of the GEMM kernel:
* [baseline.c](GEMM/baseline.c)
* [baseline_reordered.c](GEMM/baseline_reordered.c)
* [opti-l1.c](GEMM/opti-l1.c)
* [opti-l2.c](GEMM/opti-l2.c)
* [opti-l3.c](GEMM/opti-l3.c)


```
cd GEMM
# look at the C code of the different versions
make all
```

## VHDL Simulation and Synthesis

### Install VHDL simulation environments:
Check the md files depending on the VHDL simulation tool you plan to use 
* [GHDL](./GHDL_install.md)
* [ModelSim](./ModelSim_install.md)
* [EDAPlayGround](./edaplayground.md)

GHDL is recommended.

### VHDL example files
Check the files in the `VHDL` folder (e.g., halfadder, fulladder, counter)

## Slides of the course

* [Main DNN_ACC slides](slides/DNN_Accelerators.pdf)
* [VLSI Design slides](slides/VLSI_Design_Power_Primer.pdf)
* [VHDL slides](slides/VHDL_Logic_Synthesis.pdf)
* [VHDL Appendix slides](slides/VHDL_Logic_Synthesis_Appendix.pdf)



