# Installation et utilisation de Modelsim - Quartus Prime Lite Edition


## Installation sur votre machine personnelle

Pour installer les outils du flot de conception Quartus Prime Lite Edition Design Software, Version 19.1 :
https://www.intel.fr/content/www/fr/fr/software/programmable/quartus-prime/download.html

Il faut s'enregistrer en ligne au préalable, vous aurez ainsi compte qui vous permettra de faire les installations.
Les installations à télécharger sont :

- Quartus Prime Lite Edition (Free),
- Devices: Cyclone V device support.

Prenez bien la version 19.1.
Attention, il faut de la place pour installer les outils (~14 GO), par contre votre ordinateur n'a pas vraiment besoin d'être puissant.

La première fois que vous voudrez faire une simulation ModelSim depuis Quartus, vous aurez probablement un message vous disant qu'il faut préciser le chemin (répertoire) où se trouve ModelSim. Vous pourrez préciser ce répertoire dans le menu Tools/Options/EDA Tool Options.

Par exemple, avec une installation par défaut, le chemin est du type :

- sous windows : \intelFPGA_lite\19.1\modelsim_ase\win32aloem
- sous linux (redhat/centos) : ~/intelFPGA_lite/19.1/modelsim_ase/linuxaloem

Sinon lancer une recherche de l'exécutable vsim.exe pour trouver le bon répertoire.


Si, en tout début de l'installation de Quartus Prime, dans la liste des outils/devices qui vont être installés n'apparait pas ModelSim,  vous aurez alors besoin de técharger aussi ModelSim-Intel FPGA Edition pour simuler vos codes VHDL.

**Pour installer seulement ModelSim,** sous linux (redhat/centos), téléchargez le fichier `ModelSimSetup-19.1.0.670-linux.run` sur le site d'Intel : https://fpgasoftware.intel.com/19.1/?edition=lite&platform=linux dans l'onglet *Individual Files*, puis `chmod u+x ModelSimSetup-19.1.0.670-linux.run` et exécutez `./ModelSimSetup-19.1.0.670-linux.run`.

## Configuration sur les machines linux de l'ISTIC

Vérifiez que vous êtes bien en csh ou tcsh (et non en bash).

```
echo $shell
```

Si vous êtes en bash alors exécutez

```
tcsh
```

Puis, pour configurer ModelSim, exécutez : 

```
setenv LM_LICENSE_FILE 5280@mentor.cnfm.fr
echo Configuring ModelSimSE10.6c
set path=(/opt/ModelSimSE10.6c/modeltech/bin $path)

```
ou
```
source environ.csh
```

Vous devriez pouvoir lancer ModelSim :
```
vsim&
```

Pour éviter de ré-exécuter ces commandes lors de vos prochaines connexions, vous pouvez ajouter les lignes suivantes dans votre .cshrc :
```
source [path_to_environ_csh]/environ.csh
```
Pour cela `gedit ~/.cshrc` puis ajouter ces lignes à la fin du fichier.

## Tutoriel 

L'outil de simulation VHDL utilisé est ModelSim. Il vient avec l'installation de Quartus Lite et peut être utilisé sur un nombre restreint de composant FPGA. Dans le cadre du cours sur VHDL cela n'a pas d'importance car nous allons simuler directement le VHDL, sans pour l'instant chercher à le programmer sur un FPGA. Ceci sera vu plus tard lors des TP et projets du cours...

Lancez Modelsim, créez un nouveau projet grâce au menu `File/New/Project`. Insérez les fichiers nécessaires, compilez par un `Compile/Compile all`, puis lancez la simulation et insérez dans la fenêtre Wave tous les signaux intéressants pour le debbugage comme indiqué dans les rappels. 


### Rappels sur quelques points importants sur Modelsim.
- Le *prompt* dans la fenêtre *transcript* est `Modelsim>`. Si on est en mode commande et que la simulation n’est pas en cours, il est `Vsim>` si on est en mode simulation. On a alors le temps où en est la simulation en bas à côté du *Now*.
- La visualisation des chronogrammes ne se fait que si la simulation est terminée et n’est pas en mode *run*. Elle se termine si tous les process sont terminés ou si vous appuyez sur  l’icône `Stop` dans la barre d’outils.
- On peut créer un projet Modelsim qui indique les fichiers à compiler et permet de facilement recompiler par le menu `Compile/Compile all`. Ca fait gagner du temps et c’est donc conseillé.
- Si on veut pouvoir voir tous les signaux et ports de sa description, on a intérêt à lancer la simulation sans optimisation. Pour cela, lancez par le menu `Simulate/ Start simulation`. Sélectionnez le test bench et décochez `Enable optimisation` avant de lancer la simulation.
- Pensez à visualiser le maximum de signaux, ou tout du moins ceux qui vous permettent de vérifier le fonctionnement et de debbuger. 
- Pour relancer la simulation, menu `simulate/restart, (cela remet le temps de simulation à 0).
- Si vous avez inséré pleins de signaux dans la fenêtre wave et que vous ne voulez pas les perdre, mais que vous avez besoin de recompiler suite à une modification dans les sources VHDL, ne quittez pas le mode simulation, mais suspendez-le simplement par le menu `simulate/Break`. Compilez  vos  fichiers  par  le  menu  `compile`,  et  relancez  la   simulation   par   le menu `simulate/restart`. Ca peut paraitre un détail, mais ça vous fera gagner beaucoup de temps quand vous corrigerez des erreurs.

